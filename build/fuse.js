const { CSSPlugin, CSSResourcePlugin, EnvPlugin, FuseBox, ImageBase64Plugin, JSONPlugin, QuantumPlugin, SassPlugin, WebIndexPlugin } = require("fuse-box");
const { AggregateLocalesPlugin } = require('../src/lib/locale-aggregate/dist/src');
const process = require('process');

const isProduction = process.env.NODE_ENV === "production";

const fuse = FuseBox.init({
  cache: true,
  homeDir: "../src",
  target: isProduction ? "browser@es5" : "browser@es6",
  output: "../dist/$name.js",
  useJsNext: false,
  modulesFolder: './modules',
  plugins: [
    EnvPlugin(),
    ImageBase64Plugin(),
    JSONPlugin(),
    [
      SassPlugin(),
      CSSResourcePlugin({ dist: '../dist/css', inline: true }),
      CSSPlugin(),
    ],
    WebIndexPlugin({
      appendBundles: true,
      async: true,
      author: 'Ghepes Doru (Bookworm Development SRL)',
      charset: 'UTF-8',
      description: 'Irish And Music Pub Captive Portal',
      title: 'Irish And Music Pub',
      template: 'index.template.html',
    }),
    AggregateLocalesPlugin({
      locales: ['en', 'ro'],
    }),
    isProduction && QuantumPlugin({
      target: 'browser',
      // bakeApiIntoBundle: 'app',
      css: { clean: true },
      replaceProcessEnv: true,
      replaceTypeOf: true,
      treeshake: true,
      // uglify: true,
    }),
  ],
});

if (isProduction) {
  fuse
    .bundle('app')
    .instructions(" > index.tsx");

  fuse.run();
} else {
  fuse.dev(); // launch http server

  fuse
    .bundle("app")
    .instructions(" > index.tsx")
    .hmr()
    .watch();
  fuse.run();
}

import { firebaseDB } from './index';

import { firestore } from 'firebase/app';
export type DocumentReference = firestore.DocumentReference;

interface IDataObject {
  id: string;
}

/**
 * Given a collection gets the list of all contained documents
 * Does not resolve embedded DocumentReference's
 */
export async function listaAllOfCollection<T extends IDataObject>(
  collection: string, where: IWhereCondition[] = []): Promise<T[]> {
  const contents = await getAllOfCollection(collection, where);

  if (!contents.empty) {
    return contents.docs.map((d) => ({ ...d.data(), id: d.id } as T));
  }

  return [];
}

/**
 * Given a collection counts the number of documents
 */
export async function countAllOfCollection(collection: string, where: IWhereCondition[] = []): Promise<number> {
  const contents = await getAllOfCollection(collection, where);

  return contents.size;
}

/**
 * Given a collection generates a list of DocumentReference's to all contained documents
 * Filtering is supported by multiple where clauses
 */
export async function getAllOfCollection(collection: string, where: IWhereCondition[] = []) {
  if (where.length) {
    let r: firestore.Query = firebaseDB.collection(collection);

    for (const w of where) {
      r = r.where(w.fieldPath, w.relation, w.value);
    }

    return await r.get();
  }

  return await firebaseDB.collection(collection).get();
}

export async function getFirstOfCollection<T>(collection: string, where: IWhereCondition[] = []) {
  const all = await getAllOfCollection(collection, where);

  return !all.empty ? all.docs[0].data() as T : null;
}

/**
 * Resolves a DocumentReference
 */
export async function resolveReference<T>(ref: DocumentReference) {
  const value = await ref.get();

  return {
    ...value.data() as T,
    id: ref.id,
  } as T;
}

/**
 * Generates a reference string to another document in another collection
 */
export function asReferenceString(collection: string, id: string) {
  const c = collection.split('/').filter((cc) => cc.length).map((cc) => cc.trim())[0];
  const i = id.split('/').filter((ii) => ii.length).map((ii) => ii.trim())[0];

  return `/${c}/${i}`;
}

/**
 * Generates a reference string to the referenced document
 */
export function asReferenceStringFrom(ref: DocumentReference) {
  return asReferenceString(ref.parent.id, ref.id);
}

/**
 * Generates a DocumentReference
 */
export function getReferenceTo(collection: string, id: string): DocumentReference {
  return firebaseDB.doc(asReferenceString(collection, id));
}

/**
 * Given an array, id and cache (map) resolves to the entity with the specified id by either
 * hitting the cache or populating the cache till finding the entity with the specified id
 *
 * Conceptualisation:
 * Assumption: Most times data is polarized along a few connections (example authors of resources)
 * Given above assumption, it should be faster to iterate and fill the cache till finding the most referenced
 * objects and hit the cache from there after.
 *
 * Should greatly reduce the hit when compared to a cache generation and hitting the cache strategy in most
 * real life usage cases (when data has a 1 to 1 relationship the 2 might get equal)
 */
export function searchOrHitCache<T extends { id: string }>(a: T[], id: string, cache: { [id: string]: T }): T {
  const fromCache = cache[id];

  if (undefined === cache[id]) {
    // Search for the id in the array and fill it in the cache once found
    // along with any id's found along the way
    for (const aa of a) {
      cache[aa.id] = aa;

      if (aa.id === id) {
        return aa;
      }
    }
  }

  return fromCache;
}

export function searchEmbeddedOrHitCache<T extends { id: string }>(
  a: T[], id: string, cache: { [id: string]: T }, prop: keyof T,
): T {
  const fromCache = cache[id];

  if (undefined === cache[id]) {
    for (const aa of a) {
      if (aa[prop] === id as any) {
        cache[aa.id] = aa;

        return aa;
      }
    }
  }

  return fromCache;
}

interface IWhereCondition {
  fieldPath: string | firestore.FieldPath;
  relation: firestore.WhereFilterOp;
  value: any;
}

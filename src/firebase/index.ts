import * as fb from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

import { configuration } from './configuration';

// Initialize the firebase application
export const firebaseApp = fb.initializeApp(configuration, 'IrishPub');

firebaseApp.auth().useDeviceLanguage();

// Initialize the firestore database
export const firebaseDB = firebaseApp.firestore();

export * from './firebaseDB';

export const GoogleAuthProvider = new fb.auth.GoogleAuthProvider();
export const FacebookAuthProvider = new fb.auth.FacebookAuthProvider();
export const TwitterAuthProvider = new fb.auth.TwitterAuthProvider();

import { action } from '../store';
import { Delayed, DelayedFn, DelayType } from './delayed';

interface IDelayedActionOptions {
  method: DelayType;
  delay?: number;
  immediate?: boolean;
}

export function delayInAction(fn: DelayedFn, delay: number | IDelayedActionOptions = 350) {
  const options: IDelayedActionOptions =
    typeof delay !== 'number' ? delay as IDelayedActionOptions : { method: 'timeout', delay };

  return new Delayed(options.method, action(fn), options.immediate ? 0 : options.delay);
}

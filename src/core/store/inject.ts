import { inject as mobxInject } from 'mobx-preact';

import { BaseStore, IAllStores } from './base';
import { Store } from './store';

type storeAliasingFn =  (stores: IAllStores) => { [storeAlias: string]: Store; };

export function inject(...stores: string[] | storeAliasingFn[]) {
  return mobxInject(() => {
    if (stores.length && stores.length === 1 && typeof stores[0] === 'function') {
      // Alias stores as required
      const allStores = BaseStore.getInstane().getStores(BaseStore.UnfilteredStoresFilter);

      return (stores[0] as storeAliasingFn)(allStores);
    }

    // Grab the stores as normally done
    return BaseStore.getInstane().getStores(...stores as string[]);
  });
}

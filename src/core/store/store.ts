import { BaseStore } from './base';

export abstract class Store {
  public constructor() {
    BaseStore.getInstane().registerStore(this);
  }

  public abstract reset(): void;
}

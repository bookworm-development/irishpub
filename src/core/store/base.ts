import { observable } from 'mobx';

import { Store } from './store';

const UnknownStoreName = '__unnamed__';

const uniqueIdentifier = () => Math.random().toString(36).substring(2) + (new Date()).getTime().toString(36);

export class BaseStore {
  public static UnfilteredStoresFilter = uniqueIdentifier();
  public static GenerateUniqueIdentifier = uniqueIdentifier;

  public static getInstane() {
    if (!BaseStore.self) {
      BaseStore.self = new BaseStore();
    }

    return BaseStore.self;
  }

  // Allows registration of a class as a store (will initialize a first instance of the store)
  public static registerStoreClass(store: new() => Store) {
    BaseStore.getInstane().registerStoreClass(store);
  }

  private static self: BaseStore;

  @observable
  private stores: IAllStores;

  private constructor() {
    this.stores = {};
  }

  public registerStore(store: Store): Error | true;
  public registerStore(name: string, store: Store): Error | true;
  public registerStore(param1: string | Store, param2?: Store): Error | true {
    let storeName = UnknownStoreName;
    let store: Store | null = null;

    if (param1 && !param2) {
      // Store only case?
      if (typeof param1 === 'string') {
        return new Error(`Missing Store instance for intended registration of store ${param1}`);
      } else {
        const idx = param1.constructor.name.indexOf('Store');

        if (idx < 0) {
          return new Error(`Invalid store name. Stores are expected to follow the NameStore convention.`);
        } else {
          storeName = param1.constructor.name.slice(0, idx).toLowerCase();
          store = param1;
        }
      }
    } else if (param2) {
      // Store and name case?
      if (typeof param1 === 'string') {
        storeName = param1;
        store = param2;
      } else {
        return new Error(`Invalid argument for form registration, expected the Store name as first argument.`);
      }
    }

    if (!store) {
      return new Error(`Missing Store instance in store registration of ${storeName}.`);
    }

    this.stores[storeName] = store;

    return true;
  }

  public getStore(name: string) {
    return this.stores[name];
  }

  public getStores(...names: string[]) {
    const stores: IAllStores = {};

    if (names.length === 1 && names[0] === BaseStore.UnfilteredStoresFilter) {
      return this.stores;
    }

    names.forEach((n) => {
      const store = this.getStore(n);

      if (store) {
        stores[n] = store;
      }
    });

    return stores;
  }

  private registerStoreClass(store: new() => Store) {
    const param1 = store.name;
    let storeName: string = '';

    const idx = param1.indexOf('Store');

    if (idx < 0) {
      return new Error(`Invalid store name. Stores are expected to follow the NameStore convention.`);
    } else {
      storeName = param1.slice(0, idx).toLowerCase();
    }

    if (!this.stores[storeName]) {
      this.registerStore(storeName, new store());
    }
  }
}

export interface IAllStores {
  [ storeName: string ]: Store;
}

import { FormPrimitiveValue } from '../form';
import { Castings } from './casting';
import { Validator } from './validator';

export const minlengthValidator: Validator = (
  value: FormPrimitiveValue,
  configuration?: IMinLengthConfiguration) => {
  const v = Castings.string(value) as string;
  const c = { minLength: Number.MIN_SAFE_INTEGER, ...configuration };

  return !(v.length < c.minLength);
};

export interface IMinLengthConfiguration {
  minLength?: number;
}

import { FormPrimitiveValue } from '../form';
import { Castings } from './casting';
import { Validator } from './validator';

export const maxlengthValidator: Validator = (
  value: FormPrimitiveValue,
  configuration?: IMaxLengthConfiguration) => {
  const v = Castings.string(value) as string;
  const c = { maxLength: Number.MAX_SAFE_INTEGER, ...configuration };

  return !(v.length > c.maxLength);
};

export interface IMaxLengthConfiguration {
  maxLength?: number;
}

import { FormPrimitiveValue } from '../form';
import { Castings } from './casting';
import { Validator } from './validator';

export const maxValidator: Validator = (
  value: FormPrimitiveValue,
  configuration?: IMaxValidatorConfig) => {
  const v = Castings.number(value) as number;
  const c = { max: Number.MIN_SAFE_INTEGER, ...configuration };

  return !(isNaN(v) || v > c.max) ? false : true;
};

export interface IMaxValidatorConfig {
  max?: number;
}

import { FormPrimitiveValue } from '../form';
import { countValidator, ICountConfiguration } from './validator.count';
import { IMaxValidatorConfig, maxValidator } from './validator.max';
import { IMaxLengthConfiguration, maxlengthValidator } from './validator.maxlength';
import { IMinValidatorConfig, minValidator } from './validator.min';
import { IMinLengthConfiguration, minlengthValidator } from './validator.minlength';

export type Validator = (value: FormPrimitiveValue, configuration?: ValidationConfiguration) => boolean;

export type ValidationRules = 'min' | 'max' | 'minlength' | 'maxlength' | 'count';

export type ValidationConfiguration =
  IMinValidatorConfig | IMaxValidatorConfig | IMinLengthConfiguration | IMaxLengthConfiguration;

export interface IValidationConfiguration {
  count: ICountConfiguration;
  min: IMinValidatorConfig;
  max: IMaxValidatorConfig;
  minlength: IMinLengthConfiguration;
  maxlength: IMaxLengthConfiguration;
}

type ValidatorsConfiguration = {
  [rule in ValidationRules]: Validator;
};

export const Validators: ValidatorsConfiguration = {
  count: countValidator,
  max: maxValidator,
  maxlength: maxlengthValidator,
  min: minValidator,
  minlength: minlengthValidator,
};

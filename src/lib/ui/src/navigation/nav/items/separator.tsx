import { ElementComponent, h, IBaseElementComponent, observer, Omit } from '../../../core';

export interface INavSeparator extends IBaseElementComponent {
  type: 'separator';
}

@observer
export class Separator extends ElementComponent<Omit<INavSeparator, 'type'>, any> {
  protected baseClass = 'uk-nav-divider';

  public render() {
    return <li className={this.className} {...this.attributes} />;
  }
}

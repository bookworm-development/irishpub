import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { Header, Item, NavItems, Separator } from './items';

export interface INavProps extends IBaseElementComponent {
  active?: number;
  dropDown?: boolean;
  navBar?: boolean;
  styleType?: NavStyle | NavStyle[];
  collapsible?: boolean;
  multiple?: boolean;
  items?: NavItems[];
  nested?: boolean;
  accordion?: boolean;
}

type NavStyle = 'default' | 'primary' | 'center';

@observer
export class Nav extends ElementComponent<INavProps, any> {
  public static Header = Header;
  public static Item = Item;
  public static Separator = Separator;

  protected baseClass: string = '';

  public render() {
    return <ul className={this.className} {...this.attributes}>
      {
      this.props.items ? this.props.items.map((i, ii) => {
        if (i.type === 'item') {
          return <Item {...i} active={ii === this.props.active} />;
        } else if (i.type === 'header') {
          return <Header {...i} />;
        }

        return <Separator {...i} />;
      }) : null
    }
    {this.props.children}
    </ul>;
  }

  @computed
  protected get className() {
    const styles: NavStyle[] =
      this.props.styleType ?
        typeof this.props.styleType === 'string' ? [this.props.styleType] :
        this.props.styleType :
      [];

    return this.toClassName([
      super.className,

      // Main/sub-nav status
      this.props.nested ? 'uk-sub-nav' : 'uk-nav',

      // Accordion (highlight parent items)
      this.props.accordion ? 'uk-nav-parent-icon' : '',

      // Style
      ...this.props.styleType ? styles.map((s) => `uk-nav-${s}`) : [],

      // DropDown contained
      this.props.dropDown ? 'uk-dropdown-nav' : '',

      // Nav bar contained
      this.props.navBar ? 'uk-navbar-dropdown-nav' : '',
    ]);
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Accordion
      ...this.props.accordion ? {
        ['uk-nav']: this.toAttributeValue({
          collapsible: true,
          multiple: false,
          transition: 'ease',
        }, this.props),
      } : {},
    };
  }
}

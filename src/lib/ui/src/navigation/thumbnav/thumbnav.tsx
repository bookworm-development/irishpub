import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { Item, ThumbNavItem } from './thumbnav.item';

export interface IThumbNavProps extends IBaseElementComponent {
  active?: number;
  items: ThumbNavItem[];
  vertical?: boolean;
  slider?: 'slider' | 'slideshow';
}

@observer
export class ThumbNav extends ElementComponent<IThumbNavProps, any> {
  protected baseClass: string = 'uk-thumbnav';

  public render() {
    return <ul className={this.className} {...this.attributes}>
      {
        this.props.items.map((i, ii) =>
          <Item slider={this.props.slider} idx={ii} {...i} active={ii === this.props.active} />)
      }
    </ul>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Vertical orientation
      this.props.vertical ? 'uk-thumbnav-vertical' : '',
    ]);
  }
}

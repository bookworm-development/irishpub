import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { Image } from '../../images';
import { IImageProps } from '../../images/image';
import { Link } from '../../link';

interface IThumbNavItem extends IBaseElementComponent {
  location?: string;
  image: IImageProps;
  slider?: 'slider' | 'slideshow';
  idx?: number;
}

export type ThumbNavItem = IThumbNavItem;

export interface IThumbNavItemProps extends IThumbNavItem {
  active?: boolean;
}

@observer
export class Item extends ElementComponent<IThumbNavItemProps, any> {
  protected baseClass: string = '';

  public render() {
    return <li className={this.className} {...this.attributes}>
      <Link location={this.props.location || '#'}
        {
          ...this.props.slider ?
            this.props.slider === 'slider' ?
              {['uk-slider-item']: this.props.idx} :
              {['uk-slideshow-item']: this.props.idx} :
            {}
        }>
        <Image {...this.props.image} />
      </Link>
    </li>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Active
      this.props.active ? 'uk-active' : '',
    ]);
  }
}

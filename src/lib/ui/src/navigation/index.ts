export { Nav } from './nav/nav';
export { NavBar } from './navbar/navbar';
export { SubNav } from './subnav/subnav';

import * as AllSlideNav from './slidenav';
export const SlideNav = AllSlideNav;

export { ThumbNav } from './thumbnav/thumbnav';
export { IconNav } from './iconnav/iconnav';
export { DotNav } from './dotnav/dotnav';

import { ElementComponent, h, IBaseElementComponent, observer } from '../../core';

@observer
export class NavBarLeft extends ElementComponent<IBaseElementComponent, any> {
  protected baseClass: string = 'uk-navbar-left';

  public render() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }
}

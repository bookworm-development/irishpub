import { ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { NavBarSectionContents } from './navbar';

export interface INavBarCenterProps extends IBaseElementComponent {
  left?: NavBarSectionContents;
  right?: NavBarSectionContents;
}

@observer
export class NavBarCenter extends ElementComponent<INavBarCenterProps, any> {
  protected baseClass: string = 'uk-navbar-center';

  public render() {
    return <div className={this.className} {...this.attributes}>
      {this.props.left ? <div className={'uk-navbar-center-left'}>{this.props.left}</div> : null}
      {this.props.children}
      {this.props.right ? <div className={'uk-navbar-center-right'}>{this.props.right}</div> : null}
    </div>;
  }
}

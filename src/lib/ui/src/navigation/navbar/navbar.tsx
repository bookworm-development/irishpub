import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { AlignElement } from '../../core/align';
import { DropMode } from '../../core/drop';
import { NavBarCenter } from './navbar.center';
import { NavBarLeft } from './navbar.left';
import { NavBarRight } from './navbar.right';
import { NavBarToggle } from './navbar.toggle';

export interface INavBarProps extends IBaseElementComponent {
  transparent?: boolean;
  centered?: boolean;
  left?: NavBarSectionContents;
  center?: NavBarSectionContents;
  right?: NavBarSectionContents;
  options?: INavBarOptions;
}

interface INavBarOptions {
  align?: AlignElement;
  mode?: DropMode;
  ['delay-show']: number;
  ['delay-hide']: number;
  boundary: 'window';
  ['boundary-align']: boolean;
  offset: number;
  dropbar: boolean;
  ['dropbar-mode']: 'slide' | 'push';
  duration: number;
}

export type NavBarSectionContents = JSX.Element | JSX.Element[];

@observer
export class NavBar extends ElementComponent<INavBarProps, any> {
  public static ClassSubtitle = 'uk-navbar-subtitle';
  public static Toggle = NavBarToggle;

  protected baseClass: string = '';

  public render() {
    return <nav className={this.className} {...this.attributes}>
      {
        this.props.centered ?
          <NavBarCenter left={this.props.left} right={this.props.right}>
            {this.props.center}
          </NavBarCenter> :
          [
            this.props.left ? <NavBarLeft>{this.props.left}</NavBarLeft> : null,
            this.props.center ? <NavBarCenter>{this.props.center}</NavBarCenter> : null,
            this.props.right ? <NavBarRight>{this.props.right}</NavBarRight> : null,
          ]
      }
    </nav>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Add the background style
      'uk-navbar-container',

      // Transparency
      this.props.transparent ? 'uk-navbar-transparent' : '',
    ]);
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Enable the nav bar functionality
      ['uk-navbar']: this.toAttributeValue(
        {
          ['boundary-align']: false,
          ['delay-hide']: 800,
          ['delay-show']: 0,
          ['dropbar-mode']: 'slide',
          align: 'left',
          boundary: 'window',
          dropbar: false,
          duration: 200,
          mode: 'click, hover',
          offset: 0,
        } as INavBarOptions, this.props.options || {},
      ),
    };
  }
}

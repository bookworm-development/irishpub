import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { Item, SubNavItem } from './subnav.item';

export interface ISubNavProps extends IBaseElementComponent {
  active?: number;
  items: SubNavItem[];
  divider?: boolean;
  pill?: boolean;
}

@observer
export class SubNav extends ElementComponent<ISubNavProps, any> {
  protected baseClass: string = 'uk-subnav';

  public render() {
    return <ul className={this.className} {...this.attributes}>
      { this.props.items.map((i, ii) => <Item {...i} active={ii === this.props.active} />) }
      { this.props.children }
    </ul>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Divider
      this.props.divider ? 'uk-subnav-divider' : '',

      // Pill modifier
      this.props.pill ? 'uk-subnav-pill' : '',
    ]);
  }
}

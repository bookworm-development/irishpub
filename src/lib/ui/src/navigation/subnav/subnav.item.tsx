import { Containers, Icons } from '../..';
import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { Drop } from '../../core/drop';
import { Link } from '../../link';

interface ISubNavItem extends IBaseElementComponent {
  label: string;
  location?: string;
  dropDown?: {
    options: Drop;
    contents: JSX.Element | JSX.Element[];
  };
}

export type SubNavItem = ISubNavItem;

export interface ISubNavItemProps extends SubNavItem {
  active?: boolean;
}

@observer
export class Item extends ElementComponent<ISubNavItemProps, any> {
  protected baseClass: string = '';

  public render() {
    return <li className={this.className} {...this.attributes}>
      {
        <Link location={this.props.location || '#'} label={[
          this.props.label,
          this.props.dropDown ? <Icons.Icon icon={Icons.direction['triangle-down']} /> : null,
        ]} />
      }
      {
        this.props.dropDown ?
          <Containers.Tile dropDown={this.props.dropDown.options || { mode: 'click' }}>
            {this.props.dropDown.contents}
          </Containers.Tile> :
          null
      }
    </li>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Active
      this.props.active ? 'uk-active' : '',
    ]);
  }
}

import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { Link } from '../../link';

export interface ISlideNavNextProps extends IBaseElementComponent {
  large?: boolean;
  slider?: 'slider' | 'slideshow';
}

@observer
export class Next extends ElementComponent<ISlideNavNextProps, any> {
  protected baseClass: string = '';

  public render() {
    return <Link location={'#'} class={this.className} {...this.attributes} />;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Next slide navigation functionality
      ['ul-slidenav-next']: true,

      // Make the slidenav available for sliders also
      ...this.props.slider ?
        this.props.slider === 'slider' ?
          { ['uk-slider-item']: 'next' } :
          { ['uk-slideshow-item']: 'next' } :
          {},
    };
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Large
      this.props.large ? 'uk-slidenav-large' : '',
    ]);
  }
}

import { ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { Next } from './slidenav.next';
import { Previous } from './slidenav.previous';

interface ISlideNavContainerProps extends IBaseElementComponent {
  large?: boolean;
}

@observer
export class Container extends ElementComponent<ISlideNavContainerProps, any> {
  protected baseClass: string = 'uk-slidenav-container';

  public render() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
      <Next large={this.props.large} />
      <Previous large={this.props.large} />
    </div>;
  }
}

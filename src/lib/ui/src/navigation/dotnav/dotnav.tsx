import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { DotNavItem, Item } from './dotnav.item';

export interface IDotNavProps extends IBaseElementComponent {
  active?: number;
  items: DotNavItem[];
  vertical?: boolean;
}

@observer
export class DotNav extends ElementComponent<IDotNavProps, any> {
  protected baseClass: string = 'uk-dotnav';

  public render() {
    return <ul className={this.className} {...this.attributes}>
      { this.props.items.map((i, ii) => <Item {...i} active={ii === this.props.active} />) }
    </ul>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Vertical orientation
      this.props.vertical ? 'uk-dotnav-vertical' : '',
    ]);
  }
}

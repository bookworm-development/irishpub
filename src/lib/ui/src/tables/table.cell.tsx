import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface ITableCellProps extends IBaseElementComponent {
  align?: 'middle';
  heading?: boolean;
  shrink?: boolean;
  expand?: boolean;
  link?: boolean;
  colspan?: number;
  celspan?: number;
}

@observer
export class Cell extends ElementComponent<ITableCellProps, any> {
  protected baseClass = '';

  public render() {
    return this.props.heading ?
      <th className={this.className} {...this.attributes}>
        {this.props.children}
      </th> :
      <td className={this.className} {...this.attributes}>
        {this.props.children}
      </td>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Align
      this.props.align ? `uk-table-${this.props.align}` : '',

      // Shrink
      this.props.shrink ? 'uk-table-shrink' : '',

      // Expand
      this.props.expand ? 'uk-table-expand' : '',

      // Link
      this.props.link ? 'uk-table-link' : '',
    ]);
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Colspan
      ...this.props.colspan ? { colspan: this.props.colspan } : {},

      // Celspan
      ...this.props.celspan ? { celspan: this.props.celspan } : {},
    };
  }
}

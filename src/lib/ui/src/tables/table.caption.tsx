import { ElementComponent, h, IBaseElementComponent, observer } from '../core';

@observer
export class Caption extends ElementComponent<IBaseElementComponent, any> {
  protected baseClass = '';

  public render() {
    return <caption className={this.className} {...this.attributes}>
      {this.props.children}
    </caption>;
  }
}

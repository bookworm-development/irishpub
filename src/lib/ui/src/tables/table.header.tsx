import { ElementComponent, h, IBaseElementComponent, observer } from '../core';

@observer
export class Header extends ElementComponent<IBaseElementComponent, any> {
  protected baseClass = '';

  public render() {
    return <thead className={this.className} {...this.attributes}>
      {this.props.children}
    </thead>;
  }
}

import { Containers } from '..';
import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { Body } from './table.body';
import { Caption } from './table.caption';
import { Cell } from './table.cell';
import { Footer } from './table.footer';
import { Header } from './table.header';

export interface ITableProps extends IBaseElementComponent {
  divider?: boolean;
  striped?: boolean;
  hover?: boolean;
  size?: 'small' | 'large';
  justify?: boolean;
  align?: 'middle';
  stack?: boolean;
}

@observer
export class Table extends ElementComponent<ITableProps, any> {
  public static Caption = Caption;
  public static Header = Header;
  public static Footer = Footer;
  public static Body = Body;
  public static Cell = Cell;

  protected baseClass = 'uk-table';

  public render() {
    return this.props.overflow ?
      <Containers.Div overflow={this.props.overflow}>
        {this.table}
      </Containers.Div> :
      this.table;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Divider
      this.props.divider ? 'uk-table-divider' : '',

      // Striped
      this.props.striped ? 'uk-table-striped' : '',

      // Hover
      this.props.hover ? 'uk-table-hover' : '',

      // Size
      this.props.size ? `uk-table-${this.props.size}` : '',

      // Justify
      this.props.justify ? 'uk-table-justify' : '',

      // Align
      this.props.align ? `uk-table-${this.props.align}` : '',

      // Responsive
      this.props.stack ? 'uk-table-responsive' : '',
    ]);
  }

  @computed
  protected get table() {
    return <table className={this.className} {...this.attributes}>
      {this.props.children}
    </table>;
  }
}

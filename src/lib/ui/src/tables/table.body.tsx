import { ElementComponent, h, IBaseElementComponent, observer } from '../core';

@observer
export class Body extends ElementComponent<IBaseElementComponent, any> {
  protected baseClass = '';

  public render() {
    return <tbody className={this.className} {...this.attributes}>
      {this.props.children}
    </tbody>;
  }
}

import { ElementComponent, h, IBaseElementComponent, Icon, Icons, observer } from '../core';
import { Link } from '../link';

interface IMarkProps extends IBaseElementComponent {
  style: {
    left?: number | string;
    right?: number | string;
    top?: number | string;
    bottom?: number | string;
  };
}

@observer
export class Mark extends ElementComponent<IMarkProps, any> {
  protected baseClass = '';

  public render() {
    return <Link location='#' position={'absolute'} transform={'center'}
      label={
        <Icon icon={Icons.app.marker} boxShadow={'hover-xlarge'} />
      } style={this.props.style} forceAttributes={{['uk-marker']: true}} />;
  }
}

import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface IDescriptionProps extends IBaseElementComponent {
  divided?: boolean;
  items: IItem[];
}

export interface IItem {
  term: JSX.Element | JSX.Element[] | string;
  description: JSX.Element | JSX.Element[] | string;
}

@observer
export class Description extends ElementComponent<IDescriptionProps, any> {
  protected baseClass = 'uk-description-list';

  public render() {
    return <dl className={this.className} {...this.attributes}>
      { ...this.items }
    </dl>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Divider
      this.props.divided ? 'uk-description-list-divider' : '',
    ]);
  }

  @computed
  protected get items() {
    const items: JSX.Element[] = [];

    this.props.items.forEach((i) => {
      items.push(
        <dt>{i.term}</dt>,
        <dd>{i.description}</dd>,
      );
    });

    return items;
  }
}

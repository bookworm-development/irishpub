import { computed, ContainerComponent, h, IBaseContainerComponent, observer } from '../core';
import { Button, IButtonProps } from './button';

export interface IButtonGroupProps extends IBaseContainerComponent {
  buttons: IButtonProps[];
  disabled?: boolean;
}

@observer
export class ButtonGroup extends ContainerComponent<IButtonGroupProps, any> {
  protected baseClass = 'uk-button-group';

  @computed
  protected get content() {
    return <div
      className={this.className}
      {...this.attributes}>
      {
        this.props.buttons.map((conf) => {
          return <Button {...conf}
            disabled={this.props.disabled ? true : conf.disabled || false} />;
        })
      }
    </div>;
  }
}

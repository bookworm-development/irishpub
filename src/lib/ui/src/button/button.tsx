import {
  computed, ContainerComponent, ElementChildOnly, h, IBaseContainerComponent, Icon, IconType,
  MouseActionEvent, observer,
} from '../core';

export interface IButtonProps extends IBaseContainerComponent {
  type?: 'button' | 'submit' | 'reset';
  styleType?: ButtonStyle;
  size?: 'small' | 'large' | 'normal';
  label: ElementChildOnly;

  handleOn?: MouseActionEvent;
  handler: (ev: MouseEvent) => any;

  icon?: IconType;

  disabled?: boolean;

  // TODO: Add dropdown support
}

export type ButtonStyle = 'primary' | 'secondary' | 'default' | 'danger' | 'text' | 'link';

@observer
export class Button extends ContainerComponent<IButtonProps, any> {
  protected baseClass: string = 'uk-button';

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Button style
      `uk-button-${this.props.styleType || 'default'}`,

      // Button size
      this.props.size && this.props.size !== 'normal' ?
        `uk-button-${this.props.size}` : '',
    ]);
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Disabled
      ...this.props.disabled ? { disabled: true } : {},
    };
  }

  @computed
  protected get content() {
    return <button
      className={this.className}
      {...this.attributes}
      type={this.props.type || 'button'}
      {
        ...{
          [this.props.handleOn || 'onClick']: this.props.handler,
        }
      } >
      {
        this.props.icon ?
          <Icon icon={this.props.icon} ratio={.99} margin='small-right'/> : null
      }
      {this.props.label}
    </button>;
  }
}

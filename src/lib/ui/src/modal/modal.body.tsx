import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface IModalBodyProps extends IBaseElementComponent {
  center?: boolean;
  container?: boolean;
  body?: boolean;
}

@observer
export class Body extends ElementComponent<IModalBodyProps, any> {
  protected baseClass = '';

  public render() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Main container
      this.props.container ? 'uk-modal-dialog' : '',

      // Body container
      this.props.body ? 'uk-modal-body' : '',

      // Center
      this.props.center ? 'uk-margin-auto-vertical' : '',
    ]);
  }
}

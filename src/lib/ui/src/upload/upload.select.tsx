import { computed, ElementChild, ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { Link } from '../link';

interface ISelectProps extends IBaseElementComponent {
  type?: 'button' | 'link';
  label?: ElementChild;
}

@observer
export class Select extends ElementComponent<ISelectProps, any> {
  protected baseClass = '';

  public render() {
    return <div className={this.className} {...this.attributes}>
      <input type='file' multiple />
      <Link label={this.props.label}
        {...this.props.type === 'button' ? { button: true } : {}} />
    </div>;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Custom form field
      ['uk-form-custom']: true,
    };
  }
}

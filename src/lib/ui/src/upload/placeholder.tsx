import { ElementComponent, h, IBaseElementComponent, observer } from '../core';

@observer
export class Placeholder extends ElementComponent<IBaseElementComponent, any> {
  protected baseClass = 'uk-placeholder';

  public render() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }
}

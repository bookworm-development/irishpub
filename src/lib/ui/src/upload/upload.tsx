import * as UIKit from 'uikit';

import {
  action, computed, ElementChild, ElementComponent, h, IBaseElementComponent, observable, observer } from '../core';
import { Progress } from '../progress';
import { Placeholder } from './placeholder';
import { Select } from './upload.select';

interface IUploadProps extends IBaseElementComponent {
  options?: IUploadOptions;
  select?: boolean;
  label?: ElementChild;
}

type ArgLessFn = () => void;
type LoadFn = (e: ProgressEvent) => void;
interface IEnv {
  data: any;
  method: string;
  headers: { [p: string]: string; };
  xhr: XMLHttpRequest;
  responseType: string;
}
type EnvFn = (environment: IEnv) => void;

interface IUploadOptions {
  abort?: ArgLessFn;
  allow?: string;
  beforeAll?: ArgLessFn | null;
  beforeSend?: EnvFn;
  clsDragover?: string;
  complete?: ArgLessFn;
  completeAll?: ArgLessFn;
  concurrent?: number;
  error?: ArgLessFn;
  fail?: ArgLessFn;
  load?: ArgLessFn | null;
  loadEnd?: LoadFn | null;
  loadStart?: LoadFn | null;
  method?: string;
  mime?: string;
  msgInvalidMime?: string;
  msgInvalidName?: string;
  multiple?: boolean;
  name?: string;
  params?: { [p: string]: any; };
  progress?: LoadFn;
  type?: string;
  url: string;
}

@observer
export class Upload extends ElementComponent<IUploadProps, any> {
  protected baseClass = '';

  private uploadInstance: { $destroy: (element?: boolean) => void } | null = null;

  @observable
  private currentProgress: number = 0;

  @observable
  private totalProgress: number = 0;

  @observable
  private inProgress: boolean = false;

  public componentDidMount() {
    // Initialize upload component
    this.initUploader();
  }

  public componentWillUnmount() {
    if (this.uploadInstance) {
      // Unwire the upload component
      this.uploadInstance.$destroy();
    }
  }

  public render() {
    return <div>
      { this.element }
      {
        this.inProgress ?
          <Progress max={this.totalProgress} value={this.currentProgress} /> :
          null
      }
    </div>;
  }

  @computed
  protected get element() {
    return <div className={this.className} {...this.attributes}>
      {
        this.props.select ?
          <Select type={'button'} label={this.props.label} /> :
          <Placeholder text='center'>
            {this.props.children}
            {
              this.props.label ?
                <Select type={'link'} label={this.props.label} /> :
                null
            }
          </Placeholder>
      }
    </div>;
  }

  private initUploader() {
    const o = this.props.options || {} as { [p: string]: any; };

    this.uploadInstance = UIKit.upload((this.base as HTMLElement).firstElementChild, {
      ...o.allow ? { allow: o.allow } : {},
      ...o.mime ? { mime: o.mime } : {},
      ...o.name ? { name: o.name } : {},
      abort: o.abort || this.abort,
      beforeAll: o.beforeAll || this.beforeAll,
      beforeSend: o.beforeSend || this.beforeSend,
      clsDragover: o.clsDragover || 'uk-dragover',
      complete: o.complete || this.complete,
      completeAll: o.completeAll || this.completeAll,
      concurrent: o.concurrent || 1,
      error: o.error || this.error,
      fail: o.fail || this.fail,
      load: o.load || this.load,
      loadEnd: o.loadEnd || this.loadEnd,
      loadStart: o.loadStart || this.loadStart,
      method: o.method || 'POST',
      msgInvalidMime: o.msgInvalidMime || 'Invalid File Type: %s',
      msgInvalidName: o.msgInvalidName || 'Invalid File Name: %s',
      multiple: o.multiple || false,
      params: o.params || {},
      progress: o.progress || this.progress,
      type: o.type || '',
      url: o.url || '',
    } as IUploadOptions);

    console.info('Wired the upload component');
  }

  @action
  private stopProgress() {
    // Hide the progress as it's finalized
    this.inProgress = false;

    this.totalProgress = 100;
    this.currentProgress = 0;
  }

  @action.bound
  private beforeAll() {
    console.info('before all', arguments);
  }

  @action.bound
  private beforeSend(env: IEnv) {
    console.info('before send', env);

    // TODO: Add support for handling CORS here!
    // Enforce any cors policies here
    env.headers['Access-Control-Allow-Origin'] = '*';
  }

  @action.bound
  private completeAll() {
    this.stopProgress();
  }

  @action.bound
  private loadEnd() {
    console.info('load end', arguments);
  }

  @action.bound
  private loadStart(ev: ProgressEvent) {
    console.info('load start', arguments);

    // Initialize the progress bar
    this.inProgress = true;
    this.currentProgress = ev.loaded;
    this.totalProgress = ev.total;
  }

  @action.bound
  private abort() {
    this.stopProgress();
    console.info('aborted', arguments);
  }

  @action.bound
  private complete() {
    console.info('completed one', arguments);
  }

  @action.bound
  private error() {
    this.stopProgress();

    console.info('error', arguments);
  }

  @action.bound
  private fail() {
    this.stopProgress();
    console.info('fail', arguments);
  }

  @action.bound
  private load() {
    console.info('load', arguments);
  }

  @action.bound
  private progress(ev: ProgressEvent) {
    console.info('progress', arguments, ev);

    // Update the progress bar
    this.currentProgress = ev.loaded;
    this.totalProgress = ev.total;
  }
}

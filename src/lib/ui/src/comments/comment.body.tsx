import { Containers } from '..';
import { ElementComponent, h, IBaseElementComponent, observer } from '../core';

@observer
export class Body extends ElementComponent<IBaseElementComponent, any> {
  protected baseClass = 'uk-comment-body';

  public render() {
    return <Containers.Div>{this.props.children}</Containers.Div>;
  }
}

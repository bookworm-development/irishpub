import { Containers } from '..';
import { computed, ElementChild, ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { IImageProps } from '../images/image';
import { ILinkProps } from '../link/link';
import { SubNavItem } from '../navigation/subnav/subnav.item';
import { Avatar } from './comment.avatar';
import { Body } from './comment.body';
import { Header } from './comment.header';
import { Meta } from './comment.meta';
import { Title } from './comment.title';

interface ICommentProps extends IBaseElementComponent {
  avatar?: IImageProps;
  title?: ILinkProps;
  meta?: SubNavItem[];
  content?: ElementChild;
  primary?: boolean;
}

@observer
export class Comment extends ElementComponent<ICommentProps, any> {
  public static Avatar = Avatar;
  public static Header = Header;
  public static Title = Title;
  public static Meta = Meta;
  public static Body = Body;

  protected baseClass = 'uk-comment';

  public render() {
    return <article className={this.className} {...this.attributes}>
      { this.header }
      { this.content }
      {this.props.children}
    </article>;
  }

  @computed
  protected get header() {
    if (this.props.avatar || this.props.title || this.props.meta) {
      return <Header size={'medium'} flex={'middle'}>
        {
          this.props.avatar ?
            <Containers.Div width={'auto'}>
              <Avatar avatar={this.props.avatar} />
            </Containers.Div> :
            null
        }
        {
          this.props.title || this.props.meta ?
            <Containers.Div width='expand'>
              {
                this.props.title ?
                  <Title title={this.props.title} margin='remove' /> : null
              }
              {
                this.props.meta ?
                  <Meta meta={this.props.meta} /> : null
              }
            </Containers.Div> :
            null
        }
      </Header>;
    }

    return null;
  }

  @computed
  protected get content() {
    return this.props.content ?
      <Body>{this.props.content}</Body> :
      null;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Primary
      this.props.primary ? 'uk-comment-primary' : '',
    ]);
  }
}

import { ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { IImageProps, Image } from '../images/image';

interface ICommentAvatarProps extends IBaseElementComponent {
  avatar: IImageProps;
}

@observer
export class Avatar extends ElementComponent<ICommentAvatarProps, any> {
  protected baseClass = 'uk-comment-avatar';

  public render() {
    return <Image class={this.className} {...this.props.avatar} />;
  }
}

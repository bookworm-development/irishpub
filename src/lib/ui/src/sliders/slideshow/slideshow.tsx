import {
  computed, ElementComponent, h, IBaseElementComponent, observer,
} from '../../core';
import { SlideNav, ThumbNav } from '../../navigation';
import { Item, SlideshowItem } from './slideshow.item';

export interface ISlideshowProps extends IBaseElementComponent {
  options?: ISlideshowOptions;
  items: SlideshowItem[];
  slidenav?: boolean;
  dotnavigation?: boolean;
  thumbnav?: boolean;
}

interface ISlideshowOptions {
  ['autoplay-interval']?: number;
  ['max-height']: boolean | number;
  ['min-height']: boolean | number;
  ['pause-on-hover']?: boolean;
  animation?: 'slide' | 'fade' | 'scale' | 'pull' | 'push';
  autoplay?: boolean;
  draggable?: boolean;
  finite?: boolean;
  index?: number;
  ratio?: boolean | string;
  velocity?: number;
}

@observer
export class Slideshow extends ElementComponent<ISlideshowProps, any> {
  protected baseClass = '';

  public render() {
    return <div className={this.className} {...this.attributes}>
      <div className={'uk-slideshow-items'}>
        {
          this.props.items.map((i) => <Item {...i} />)
        }
      </div>

      { ...this.sidenav }
      { this.dotnavigation }
      { this.thumbnav }
    </div>;
  }

  @computed
  protected get dotnavigation() {
    return this.props.dotnavigation ?
      <ul className={'uk-slideshow-nav uk-dotnav'}></ul> : null;
  }

  @computed
  protected get sidenav() {
    return this.props.slidenav ?
      [
        <SlideNav.Previous slider='slideshow' />,
        <SlideNav.Next slider='slideshow' />,
      ] : [];
  }

  @computed
  protected get thumbnav() {
    return this.props.thumbnav ?
      <ThumbNav slider='slideshow' items={this.props.items.map((i) => ({
        image: (i as any).image,
      }))} /> : null;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Slider functionality
      ['uk-slideshow']: this.toAttributeValue(
        {
          ['autoplay-interval']: 7000,
          ['max-height']: false,
          ['min-height']: false,
          ['pause-on-hover']: true,
          animation: 'slide',
          autoplay: false,
          draggable: true,
          finite: false,
          index: 0,
          ratio: '16:9',
          velocity: 1,
        } as ISlideshowOptions,
        this.props.options || {},
      ),
    };
  }
}

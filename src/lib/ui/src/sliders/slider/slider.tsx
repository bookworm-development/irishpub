import {
  computed, ElementComponent, h, IBaseElementComponent, observer,
  WidthChildElement, WidthChildElementResponsive,
} from '../../core';
import { SlideNav, ThumbNav } from '../../navigation';
import { SliderItem } from './slider.item';
import { SliderList } from './slider.list';

export interface ISliderProps extends IBaseElementComponent {
  options?: ISliderOptions;
  items: SliderItem[];
  childWidth?: WidthChildElement | WidthChildElementResponsive;
  glutter?: boolean;
  slidenav?: boolean;
  navigationOutside?: boolean;
  dotnavigation?: boolean;
  thumbnav?: boolean;
}

interface ISliderOptions {
  autoplay?: boolean;
  ['autoplay-interval']?: number;
  center?: boolean;
  draggable?: boolean;
  finite?: boolean;
  index?: number;
  ['pause-on-hover']?: boolean;
  sets?: boolean;
  velocity?: number;
}

@observer
export class Slider extends ElementComponent<ISliderProps, any> {
  protected baseClass = '';

  public render() {
    return <div className={this.className} {...this.attributes}>
      {
        this.props.navigationOutside ?
        <div className='uk-position-relative'>
          <div className='uk-slider-container'>
            <SliderList items={this.props.items} childWidth={this.props.childWidth} />
          </div>

          { this.sidenav }
          { this.dotnavigation }
          { this.thumbnav }
        </div> :
        [
          <SliderList items={this.props.items} childWidth={this.props.childWidth} />,

          ...this.sidenav,
          this.dotnavigation,
          this.thumbnav,
        ]
      }
    </div>;
  }

  @computed
  protected get dotnavigation() {
    return this.props.dotnavigation ?
      <ul className={'uk-slider-nav uk-dotnav'}></ul> : null;
  }

  @computed
  protected get sidenav() {
    return this.props.slidenav ?
      [
        <SlideNav.Previous slider='slider' />,
        <SlideNav.Next slider='slider' />,
      ] : [];
  }

  @computed
  protected get thumbnav() {
    return this.props.thumbnav ?
      <ThumbNav slider='slider' items={this.props.items.map((i) => ({
        image: (i as any).image,
      }))} /> : null;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Slider functionality
      ['uk-slider']: this.toAttributeValue(
        {
          autoplay: false,
          ['autoplay-interval']: 7000,
          center: false,
          draggable: true,
          finite: false,
          index: 0,
          ['pause-on-hover']: true,
          sets: false,
          velocity: 1,
        },
        this.props.options || {},
      ),
    };
  }
}

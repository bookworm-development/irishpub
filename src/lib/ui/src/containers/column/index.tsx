import { BaseComponent, h, IBaseContainerComponent, observer } from '../../core';
import { Width } from '../../core';
import { Column } from './column';

export { Column } from './column';
export { Span } from './column.span';

function columnFactory(width: Width) {
  return observer(class ConcreteColumn extends BaseComponent<IBaseContainerComponent, any> {
    protected baseClass = '';

    public render() {
      return <Column {...this.props} width={width} />;
    }
  });
}

export const Columns2 = columnFactory('1-2');
export const Columns3 = columnFactory('1-3');
export const Columns4 = columnFactory('1-4');
export const Columns5 = columnFactory('1-5');
export const Columns6 = columnFactory('1-6');

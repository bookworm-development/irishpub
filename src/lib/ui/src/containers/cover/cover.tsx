import {
  computed, ContainerComponent, h, IBaseContainerComponent, observer,
} from '../../core';
import { IImageProps, Image } from '../../images/image';
import { IVideoProps, Video } from '../../video/video';
import { IIFrame } from './iframe.interface';

interface IImage extends IImageProps {
  type: 'Image';
}

interface IVideo extends IVideoProps {
  type: 'Video';
}

export interface ICoverProps extends IBaseContainerComponent {
  responsive?: { width: number; height: number };
  overlayContent?: boolean; // Allows children to be overlayed over the video or image
  content: IImage | IVideo  | IIFrame | JSX.Element;
}

@observer
export class Cover extends ContainerComponent<ICoverProps, any> {
  protected baseClass: string = 'uk-cover-container';

  @computed
  protected get content() {
    return <div className={this.className} {...this.attributes}>
      { this.overlay }
      { this.canvas }
      { this.resource }
    </div>;
  }

  @computed
  protected get canvas() {
    return this.props.responsive ?
      <canvas
        width={this.props.responsive.width}
        height={this.props.responsive.height} /> :
      null;
  }

  @computed
  protected get overlay() {
    return this.props.overlayContent ?
      <div className={'uk-position-top uk-height-1-1'} style={{ zIndex: 1 }}>
        {this.props.children}
      </div> :
      null;
  }

  @computed
  protected get resource() {
    if (this.props.content) {
      if (this.props.content instanceof Element) {
        // Return the received content element as is
        return this.props.content;
      } else {
        const content = this.props.content as (IVideo | IIFrame | IImage);

        if (content.type === 'Video') {
          return <Video {...content as IVideoProps} forceAttributes={{ ['uk-cover']: true }} />;
        } else if (content.type === 'IFrame') {
          return this.iframe;
        } else if (content.type === 'Image') {
          return <Image {...content as IImageProps} forceAttributes={{ ['uk-cover']: true }} />;
        }
      }
    }

    return null;
  }

  @computed
  private get iframe() {
    const i: IIFrame = this.props.content as IIFrame;
    const attrs = {
      ...i,
    };

    delete attrs.type;

    return <iframe {...attrs} uk-cover />;
  }
}

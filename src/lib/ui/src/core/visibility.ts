export type Visibility = 'hidden' | 'invisible';

export function toVisibilityClasses(visibility: Visibility | undefined) {
  if (visibility === 'invisible') {
    return 'uk-invisible';
  }

  return '';
}

export function toVisibilityAttribute(visibility: Visibility | undefined) {
  if (visibility === 'hidden') {
    return {
      hidden: true,
    };
  }

  return {};
}

export type FlexElement = 'inline';

export type FlexHorizontalAlignmnet = 'left' | 'center' | 'right' | 'between' | 'around';
export type FlexHorizontalAlignmnetResponsive =
  'left@s' | 'center@s' | 'right@s' | 'between@s' | 'around@s' |
  'left@m' | 'center@m' | 'right@m' | 'between@m' | 'around@m' |
  'left@l' | 'center@l' | 'right@l' | 'between@l' | 'around@l' |
  'left@xl' | 'center@xl' | 'right@xl' | 'between@xl' | 'around@xl';

export type FlexVerticalAlignment = 'stretch' | 'top' | 'middle' | 'bottom';

export type FlexDirectionModifier = 'row' | 'row-reverse' | 'column' | 'column-reverse';

export type FlexWrap = 'wrap' | 'wrap-reverse' | 'nowrap';

export type FlexWrapAlignment =
  'wrap-stretch' | 'wrap-between' | 'wrap-around' | 'wrap-top' | 'wrap-middle' | 'wrap-bottom';

export type FlexItemOrder = 'first' | 'last';
export type FlexItemOrderResponsive =
  'first@s' | 'first@m' | 'first@l' | 'first@xl' |
  'last@s' | 'last@m' | 'last@l' | 'last@xl';

export type FlexItemDimension = 'none' | 'auto' | '1';

export type Flex =
  FlexElement |
  FlexHorizontalAlignmnet | FlexHorizontalAlignmnetResponsive | FlexVerticalAlignment |
  FlexDirectionModifier | FlexWrap | FlexWrapAlignment |
  FlexItemOrder | FlexItemOrderResponsive | FlexItemDimension;

// Generates a class name based on provided width(s)
export function toFlexClasses(
  flex: Flex | Flex[] | undefined,
  prefix: string = '') {
  const f: Flex[] = flex ?
    typeof flex === 'string' ? [flex] : flex : [];

  const flexed = f.filter((ff) => !!ff);

  if (flexed.length) {
    return `uk-flex ${
      flexed.map((ff) => `${ prefix ? prefix : 'uk-flex-' }${ff}`).join(' ')
    }`;
  }

  return '';
}

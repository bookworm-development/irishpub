export type AnimationType =
  'fade' |
  'scale-up' | 'scale-down' |
  'slide-top' | 'slide-bottom' | 'slide-left' | 'slide-right' |
  'slide-top-small' | 'slide-bottom-small' | 'slide-left-small' | 'slide-right-small' |
  'slide-top-medium' | 'slide-bottom-medium' | 'slide-left-medium' | 'slide-right-medium' |
  'kenburns' | 'shake';

export type AnimationToggle = 'toggle';

export type AnimationModifiers = 'reverse' | 'fast';

export type Animation =
  AnimationType | AnimationToggle | AnimationModifiers;

// Generates a class name based on provided width(s)
export function toAnimationClasses(
  animation: Animation | Animation[] | undefined,
  prefix: string = '') {
  const a: Animation[] = animation ?
    typeof animation === 'string' ? [animation] : animation : [];

  return a.
    filter((aa) => !!aa).
    map((aa) => `${ prefix ? prefix : 'uk-animation-' }${aa}`).join(' ');
}

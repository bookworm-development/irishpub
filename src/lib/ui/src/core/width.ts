export type WidthElement = '1-1' | '1-2' | '1-3' | '2-3' | '1-4' | '2-4' | '3-4' | '1-5' | '2-5' | '3-5' | '4-5' |
  '1-6' | '2-6' | '3-6' | '4-6' | '5-6';

export type WidthChildElement =
  'child-1-1' | 'child-1-2' | 'child-1-3' | 'child-1-4' | 'child-1-5' | 'child-1-6' |
  'child-auto' | 'child-expand';

export type WidthElementResponsive =
  '1-1@s' | '1-2@s' | '1-3@s' | '2-3@s' | '1-4@s' | '2-4@s' | '3-4@s' | '1-5@s' | '2-5@s' | '3-5@s' | '4-5@s' |
  '1-6@s' | '2-6@s' | '3-6@s' | '4-6@s' | '5-6@s' |
  '1-1@m' | '1-2@m' | '1-3@m' | '2-3@m' | '1-4@m' | '2-4@m' | '3-4@m' | '1-5@m' | '2-5@m' | '3-5@m' | '4-5@m' |
  '1-6@m' | '2-6@m' | '3-6@m' | '4-6@m' | '5-6@m' |
  '1-1@l' | '1-2@l' | '1-3@l' | '2-3@l' | '1-4@l' | '2-4@l' | '3-4@l' | '1-5@l' | '2-5@l' | '3-5@l' | '4-5@l' |
  '1-6@l' | '2-6@l' | '3-6@l' | '4-6@l' | '5-6@l' |
  '1-1@xl' | '1-2@xl' | '1-3@xl' | '2-3@xl' | '1-4@xl' | '2-4@xl' | '3-4@xl' |
  '1-5@xl' | '2-5@xl' | '3-5@xl' | '4-5@xl' |
  '1-6@xl' | '2-6@xl' | '3-6@xl' | '4-6@xl' | '5-6@xl';

export type WidthChildElementResponsive =
  'child-1-1@s' | 'child-1-1@m' | 'child-1-1@l' | 'child-1-1@xl' |
  'child-1-2@s' | 'child-1-2@m' | 'child-1-2@l' | 'child-1-2@xl' |
  'child-1-3@s' | 'child-1-3@m' | 'child-1-3@l' | 'child-1-3@xl' |
  'child-1-4@s' | 'child-1-4@m' | 'child-1-4@l' | 'child-1-4@xl' |
  'child-1-5@s' | 'child-1-5@m' | 'child-1-5@l' | 'child-1-5@xl' |
  'child-1-6@s' | 'child-1-6@m' | 'child-1-6@l' | 'child-1-6@xl' |
  'child-auto@s' | 'child-auto@m' | 'child-auto@l' | 'child-auto@xl' |
  'child-expand@s' | 'child-expand@m' | 'child-expand@l' | 'child-expand@xl';

export type WidthComputed = 'auto' | 'expand';
export type WidthComputedResponsive =
  'auto@s' | 'auto@m' | 'auto@l' | 'auto@xl' |
  'expand@s' | 'expand@m' | 'expand@l' | 'expand@xl';

export type WidthChild = WidthElement;
export type WidthChildResponsive = WidthElementResponsive;

export type WidthFixed = 'small' | 'medium' | 'large' | 'xlarge' | 'xxlarge';

export type Width =
  WidthElement | WidthChildElement | WidthElementResponsive | WidthChildElementResponsive |
  WidthComputed | WidthChildResponsive |
  WidthChild | WidthChildResponsive | WidthFixed;

// Generates a class name based on provided width(s)
export function toWidthClasses(
  widths: Width | Width[] | undefined,
  prefix: string = '') {
  const w: Width[] = widths ?
    typeof widths === 'string' ? [widths] : widths :
    [];

  return w.
    filter((ww) => !!ww).
    map((ww) => {
      const isChildContext = ww.indexOf('child-') === 0;
      const www = isChildContext ? ww.slice(6) : ww;

      return `${
        prefix ? prefix :
        isChildContext ?
          'uk-child-width-' : 'uk-width-'
      }${www}`;
    }).
    join(' ');
}

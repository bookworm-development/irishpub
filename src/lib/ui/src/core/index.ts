export { action, computed, observable } from 'mobx';
export { observer } from 'mobx-preact';
export { Component, h } from 'preact';

export * from './icons';

export { IValidityStatus } from './validitystatus';
export * from './events';
export * from './component';
export * from './width';

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type ElementChild = ElementChildOnly | ElementChildOnly[];
export type ElementChildOnly = null | string | JSX.Element;

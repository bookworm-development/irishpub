import { action, IComputedValue } from 'mobx';

import { IValidityStatus } from '../validitystatus';
import { ElementComponent, IBaseElementComponent } from './element';

import { ValueUpdateEvent } from '../events';

export interface IBaseFormComponent extends IBaseElementComponent {
  name: string;
  value: IComputedValue<string | boolean | number | boolean[]>;

  // updateHandler: (name: string, value: string) => void;
  updateOn?: ValueUpdateEvent;

  blank?: boolean;
  disabled?: boolean;
  formWidth?: FormElementWidth;
  size?: 'small' | 'medium' | 'large';
  validity?: IValidityStatus;

  autocomplete?: 'true' | 'soft' | 'off';
  autofocus?: boolean;
  readonly?: boolean;
}

type FormElementWidth = 'large' | 'medium' | 'small' | 'xsmall';

export abstract class FormComponent<P extends IBaseFormComponent, S> extends ElementComponent<P, S> {
  /**
   * Generates the list of class names that can be attached to a form component
   */
  protected get className() {
    return this.toClassName([
      super.className,

      // Blank
      this.props.blank ? 'uk-form-blank' : '',

      // Form width
      this.props.formWidth ? `uk-form-width-${this.props.formWidth}` : '',

      // Size modifier
      this.props.size && this.props.size !== 'medium' ? `uk-form-${this.props.size}` : '',

      // Validity status
      this.props.validity ?
        this.props.validity.valid === true ? ' uk-form-success' :
        this.props.validity.valid === false ? ' uk-form-danger' : '' : '',
    ]);
  }

  /**
   * Generates the attributes map associated with a container component
   */
  protected get attributes() {
    return {
      ...super.attributes,

      // Autocomplete
      ...this.props.autocomplete ? { autocomplete: this.props.autocomplete } : {},

      // Autofocus
      ...this.props.autofocus ? { autofocus: this.props.autofocus } : {},

      // Disabled
      ...this.props.disabled ? { disabled: this.props.disabled } : {},

      // Readonly
      ...this.props.readonly ? { readonly: this.props.readonly } : {},

      // Update event type and generic handler
      [this.props.updateOn as string || 'onChange']: this.updateValue,
    };
  }

  @action.bound
  protected updateValue(event: any) {
    // Update the outer value
    if (event.target.type === 'checkbox' || event.target.type === 'radio') {
      // Checkbox and radio updates (use on|off as true|false)
      this.props.value.set(event.target.checked);
    } else {
      // Normal update
      this.props.value.set(event.target.value);
    }
  }
}

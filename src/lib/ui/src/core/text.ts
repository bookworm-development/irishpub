export type TextModifier = 'lead' | 'meta';

export type TextSize = 'small' | 'large';

export type TextWeight = 'bold';

export type TextTransform = 'uppercase' | 'capitalize' | 'lowercase';

export type TextColor = 'muted' | 'emphasis' | 'primary' | 'success' | 'warning' | 'danger';

export type TextBackground = 'background';

export type TextAlign = 'left' | 'right' | 'center' | 'justify';

export type TextResponsive =
  'left@s' | 'center@s' | 'right@s' |
  'left@m' | 'center@m' | 'right@m' |
  'left@l' | 'center@l' | 'right@l' |
  'left@xl' | 'center@xl' | 'right@xl';

export type TextVerticalAligmnment =
  'top' | 'middle' | 'bottom' | 'baseline';

export type TextWrapping = 'truncate' | 'break' | 'nowrap';

export type Text =
  TextModifier | TextSize | TextWeight | TextTransform | TextBackground | TextAlign | TextColor |
  TextResponsive | TextVerticalAligmnment | TextWrapping;

export function toTextClasses(
  text: Text | Text[] | undefined) {
  const t: Text[] = text ?
    typeof text === 'string' ? [text] : text : [];

  return t.
    filter((tt) => !!tt).
    map((tt) => `uk-text-${tt}`).
    join(' ');
}

interface IToggle {
  target?: string;
  mode?: 'hover' | 'click' | 'media';
  cls?: string;
  animation?: string[];
  duration?: number;
  queued?: boolean;
}

export type Toggle = IToggle;

export function toToggleAttribute(toggle: Toggle | undefined) {
  if (toggle) {
    return {
      target: toggle.target,
      ...toggle.mode ? { mode: toggle.mode } : {},
      ...toggle.cls ? { cls: toggle.cls } : {},
      ...toggle.animation ? { animation: toggle.animation } : {},
      ...toggle.duration ? { duration: toggle.duration } : {},
      ...toggle.queued ? { queued: toggle.queued } : {},
    };
  }

  return {};
}

import { IconType } from '.';
import { computed, h, observer } from '..';
import { ElementComponent, IBaseElementComponent } from '../component/element';

interface IIconProps extends IBaseElementComponent {
  icon: IconType;
  ratio?: number;
}

@observer
export class Icon extends ElementComponent<IIconProps, any> {
  protected baseClass = '';

  public render() {
    return <span {...this.attributes} className={this.className} />;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      ['uk-icon']: this.toAttributeValue(
        {
          icon: 'check',
          ratio: 1,
        },
        this.props,
        true,
      ),
    };
  }
}

export interface IValidityStatus {
  reason?: string;
  valid: boolean;
}

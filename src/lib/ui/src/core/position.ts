export type PositionElement = 'top' | 'left' | 'right' | 'bottom';

export type PositionElementXY =
  'top-left' | 'top-center' | 'top-right' |
  'center' | 'center-left' | 'center-right' |
  'bottom-left' | 'bottom-center' | 'bottom-right';

export type PositionCover = 'cover';

export type PositionSize = 'small' | 'medium' | 'large';

export type PositionOutside = 'center-left-out' | 'center-right-out';

export type PositionUtility = 'relative' | 'absolute' | 'fixed' | 'z-index';

export type Position =
  PositionElement | PositionElementXY | PositionCover | PositionOutside | PositionSize | PositionUtility;

export function toPositionClasses(
  position: Position | Position[] | undefined) {
  const p: Position[] = position ?
    typeof position === 'string' ? [position] : position : [];

  return p.
    filter((pp) => !!pp).
    map((pp) => `uk-position-${pp}`).
    join(' ');
}

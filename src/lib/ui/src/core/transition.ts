export type Transition =
  'toggle' |
  'fade' |
  'scale-up' | 'scale-down' |
  'slide-top' | 'slide-bottom' | 'slide-left' | 'slide-right' |
  'slide-top-small' | 'slide-bottom-small' | 'slide-left-small' | 'slide-right-small' |
  'slide-top-medium' | 'slide-bottom-medium' | 'slide-left-medium' | 'slide-right-medium';

export function toTransitionClasses(transition: Transition | Transition[] | undefined) {
  const t: Transition[] = transition ?
    typeof transition === 'string' ? [transition] : transition :
    [];

  return t.
    filter((tt) => !!tt).
    map((tt) => `uk-transition-${tt}`).
    join(' ');
}

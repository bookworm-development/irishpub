export type HeightSize = '1-1' | 'small' | 'medium' | 'large' | 'viewport';

export interface IHeightViewport {
  ['offset-top']?: boolean;
  ['offset-bottom']?: boolean | number | string;
  expand?: boolean;
  ['min-height']?: number;
}

export type HeightViewportAttribute = keyof IHeightViewport;

export interface IHeightMatch {
  target?: string;
  row?: boolean;
}

export type HeightMatchAttribute = keyof IHeightMatch;

export type Height = HeightSize;

export function toHeightClasses(
  height: Height | undefined) {
  if (typeof height === 'string') {
    return `uk-height-${height}`;
  }

  return '';
}

export function toViewportAttribute(attr: IHeightViewport) {
  if (!attr) {
    return {};
  }

  if (typeof attr === 'string') {
    if (attr === 'viewport')  {
      return { ['uk-height-viewport']: '' };
    }
  } else {
    const attributes = Object.keys(attr || {}).
      map((aa: HeightViewportAttribute) => `${aa}: ${attr[aa]}`);

    return attributes.length ? { ['uk-height-viewport']: attributes.join(', ') } : {};
  }
}

export function toMatchAttribute(attr: IHeightMatch) {
  const attributes = Object.keys(attr || {}).
    map((aa: HeightMatchAttribute) => `${aa}: ${attr[aa]}`);

  return attributes.length ? { ['uk-height-match']: attributes.join(', ') } : {};
}

import { INotificationOptions } from './notification';
import { NotificationsGroup } from './notification.group';

export class NotificationManager {
  private groups: { [groupName: string]: NotificationsGroup };

  public constructor() {
    this.groups = {
      default: new NotificationsGroup('default'),
    };
  }

  // Gets a reference to a NotificationGroup or returns the default one
  public group(groupName: string) {
    return this.groups[groupName] || this.groups.default;
  }

  // Creates a new group
  public newGroup(groupName: string) {
    if (this.groups[groupName]) {
      return this.groups[groupName];
    }

    const group = new NotificationsGroup(groupName);
    this.groups[groupName] = group;

    return group;
  }

  // Creates a new notification in specified group or the default one
  public new(options: INotificationOptions, groupName: string = 'default') {
    return this.group(groupName).new(options);
  }

  // Close all notifications in all groups
  public closeAll(immediate: boolean = false) {
    Object.keys(this.groups).forEach((g) => {
      this.groups[g].closeAll(immediate);
    });
  }
}

import { Grid } from '../containers';
import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface ICountDownProps extends IBaseElementComponent {
  date: Date;
  elements?: IItem[];
  separator?: JSX.Element[] | JSX.Element | boolean;
}

type CountDownElements  = 'days' | 'hours' | 'minutes' | 'seconds';

interface IItem {
  type: CountDownElements;
  label?: JSX.Element[] | JSX.Element | string;
}

@observer
export class CountDown extends ElementComponent<ICountDownProps, any> {
  public static DefaultElements: IItem[] = [
    { type: 'days' }, { type: 'hours' }, { type: 'minutes' }, { type: 'seconds' },
  ];

  private static ClassElementPrefix = 'uk-countdown-';

  private static DefaultSeparator = ':';

  private static ClassNumber = 'uk-countdown-number';
  private static ClassSeparator = 'uk-countdown-separator';
  private static ClassLabel = 'uk-countdown-label';

  protected baseClass = '';

  public render() {
    return <Grid size={'small'} width={'child-auto'} {...this.props} {...this.attributes}
      forceAttributes={{ ['uk-countdown']: `date: ${this.props.date.toISOString()}` }}>
      { ...this.elements }
    </Grid>;
  }

  @computed
  protected get elements() {
    const elements: Array<JSX.Element | null> = [];
    const provided = this.props.elements as IItem[] || CountDown.DefaultElements;
    const n = provided.length - 1;

    provided.forEach((e, i) => {
      // Add the element
      elements.push(
        <div>
          <div className={`${CountDown.ClassNumber} ${CountDown.ClassElementPrefix}${e.type}`}></div>
          { this.label(e.label) }
        </div>,
      );

      // Add a separator after all elements except the last one
      if (i !== n) {
        elements.push(this.separator);
      }
    });

    return elements;
  }

  @computed
  protected get separator() {
    const separator: Array<JSX.Element | null | string> =
      typeof this.props.separator === 'boolean' ?
        [CountDown.DefaultSeparator] :
        this.props.separator instanceof Array ? this.props.separator :
          this.props.separator ? [this.props.separator] :
      [null];

    if (separator[0] === null) {
      return null;
    }

    return <div className={CountDown.ClassSeparator}>
      {...separator}
    </div>;
  }

  protected label(l: IItem['label']) {
    const label: Array<JSX.Element | null | string> =
      typeof l === 'string' ?
        [l] :
        l instanceof Array ? l :
          l ? [l] :
      [null];

    if (label[0] === null) {
      return null;
    }

    return <div className={`${CountDown.ClassLabel} uk-margin-small uk-text-center`}>
      {...label}
    </div>;
  }
}

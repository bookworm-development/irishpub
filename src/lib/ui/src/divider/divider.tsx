import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface IDividerProps extends IBaseElementComponent {
  icon?: boolean;
  size?: 'small';
  vertical?: boolean;
}

@observer
export class Divider extends ElementComponent<IDividerProps, any> {
  protected baseClass = '';

  public render() {
    return <hr class={this.className} {...this.attributes} />;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Icon
      this.props.icon ? 'uk-divider-icon' : '',

      // Size
      this.props.size ? `uk-divider-${this.props.size}` : '',

      // Vertical
      this.props.vertical ? 'uk-divider-vertical' : '',
    ]);
  }
}

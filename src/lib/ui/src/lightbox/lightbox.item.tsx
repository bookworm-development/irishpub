import { computed, ElementComponent, h, observer } from '../core';
import { ILinkProps, Link } from '../link/link';

interface IItemProps extends ILinkProps {
  alt?: string;
  caption?: string;
  poster?: string;
  type?: 'iframe' | 'video' | 'image';
}

@observer
export class Item extends ElementComponent<IItemProps, any> {
  protected baseClass = '';

  public render() {
    return <Link {...this.linkProps} forceAttributes={this.attributes} />;
  }

  @computed
  protected get linkProps(): ILinkProps {
    const props: IItemProps = { ...this.props };

    if (typeof props.alt !== 'undefined') {
      delete props.alt;
    }

    if (typeof props.caption !== 'undefined') {
      delete props.caption;
    }

    if (typeof props.poster !== 'undefined') {
      delete props.poster;
    }

    if (typeof props.type !== 'undefined') {
      delete props.type;
    }

    return props;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Alt
      ...this.props.alt ? { ['data-alt']: this.props.alt } : {},

      // Caption
      ...this.props.caption ? { ['data-caption']: this.props.caption } : {},

      // Poster
      ...this.props.poster ? { ['data-poster']: this.props.poster } : {},

      // Type
      ...this.props.type ? { ['data-type']: this.props.type } : {},
    };
  }
}

import * as AllBreadcrumbs from './breadcrumb';
import * as AllButtons from './button';
import * as AllComments from './comments';
import * as AllContainers from './containers';
import * as AllFormComponents from './form';
import * as AllHeadings from './headings';
import * as AllImages from './images';
import * as AllLists from './lists';
import * as AllNav from './navigation';
import * as AllOverlays from './overlays';
import * as AllSliders from './sliders';
import * as AllTables from './tables';
import * as AllUpload from './upload';
import * as AllVideo from './video';

import { Icon, Icons as AllIcons } from './core';

export const Breadcrumbs = AllBreadcrumbs;
export const Buttons = AllButtons;
export const Containers = AllContainers;
export const Comments = AllComments;
export const Form = AllFormComponents;
export const Headings = AllHeadings;
export const Images = AllImages;
export const Lists = AllLists;
export const Navigation = AllNav;
export const Overlays = AllOverlays;
export const Sliders = AllSliders;
export const Tables = AllTables;
export const Uploads = AllUpload;
export const Videos = AllVideo;

export { IFormProps } from './form';

export const Icons = {
  ...AllIcons,
  Icon,
};

export { Alert } from './alert';
export { Badge } from './badge';
export { Close } from './close';
export { CountDown } from './countdown';
export { Divider } from './divider';
export { Label } from './label';
export { Leader } from './leader';
export { LightBox } from './lightbox';
export { Link } from './link';
export { Logo } from './logo';
export { Mark } from './mark';
export { Modal } from './modal';
export { Notifications } from './notification';
export { OffCanvas } from './offcanvas';
export { Pagination } from './pagination';
export { Progress } from './progress';
export { Tab } from './tab';

export * from './core';

import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

interface ILeaderProps extends IBaseElementComponent {
  fill?: string;
  media?: string | number;
}

@observer
export class Leader extends ElementComponent<ILeaderProps, any> {
  private static DefaultAttributeDefaults = {
    fill: '.',
  };

  protected baseClass = '';

  public render() {
    return <div class={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Fill the space available
      'uk-width-expand',
    ]);
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      ['uk-leader']: this.toAttributeValue(Leader.DefaultAttributeDefaults, this.props),
    };
  }
}

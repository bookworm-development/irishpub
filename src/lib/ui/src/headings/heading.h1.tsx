import { h } from '../core';
import { Heading, IHeaderProps } from './heading';

export class H1<P extends IHeaderProps, S> extends Heading<P, S> {
  public render() {
    return <h1 className={this.className} {...this.attributes}>{this.props.children}</h1>;
  }
}

import { h } from '../core';
import { Heading, IHeaderProps } from './heading';

export class H4<P extends IHeaderProps, S> extends Heading<P, S> {
  public render() {
    return <h4 className={this.className} {...this.attributes}>{this.props.children}</h4>;
  }
}

import { h } from '../core';
import { Heading, IHeaderProps } from './heading';

export class H2<P extends IHeaderProps, S> extends Heading<P, S> {
  public render() {
    return <h2 className={this.className} {...this.attributes}>{this.props.children}</h2>;
  }
}

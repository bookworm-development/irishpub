export { H1 } from './heading.h1';
export { H2 } from './heading.h2';
export { H3 } from './heading.h3';
export { H4 } from './heading.h4';
export { H5 } from './heading.h5';
export { H6 } from './heading.h6';

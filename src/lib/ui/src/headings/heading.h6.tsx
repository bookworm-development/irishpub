import { h } from '../core';
import { Heading, IHeaderProps } from './heading';

export class H6<P extends IHeaderProps, S> extends Heading<P, S> {
  public render() {
    return <h6 className={this.className} {...this.attributes}>{this.props.children}</h6>;
  }
}

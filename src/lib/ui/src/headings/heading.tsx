import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface IHeaderProps extends IBaseElementComponent {
  bullet?: boolean;
  divider?: boolean;
  hero?: boolean;
  line?: boolean;
  primary?: boolean;
}

@observer
export class Heading<P extends IHeaderProps, S> extends ElementComponent<P, S> {
  private static ClassBullet = 'uk-heading-bullet';
  private static ClassDivider = 'uk-heading-divider';
  private static ClassHero = 'uk-heading-hero';
  private static ClassLine = 'uk-heading-line';
  private static ClassPrimary = 'uk-heading-primary';

  protected baseClass = '';

  public render() {
    return <div class={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Bullet
      this.props.bullet ? Heading.ClassBullet : '',

      // Divider
      this.props.divider ? Heading.ClassDivider : '',

      // Hero
      this.props.hero ? Heading.ClassHero : '',

      // Line
      this.props.line ? Heading.ClassLine : '',

      // Primary
      this.props.primary ? Heading.ClassPrimary : '',
    ]);
  }
}

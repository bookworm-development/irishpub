import { ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface IProgressProps extends IBaseElementComponent {
  value: number;
  max?: number;
}

@observer
export class Progress extends ElementComponent<IProgressProps, any> {
  protected baseClass = 'uk-progress';

  public render() {
    return <progress className={this.className} {...this.attributes}
      max={this.props.max || 100}
      value={this.props.value} />;
  }
}

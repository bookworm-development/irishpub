import {
  computed, ElementChild, ElementComponent, h, IBaseElementComponent, observer,
} from '../core';

export interface IFormLabelProps extends IBaseElementComponent {
  label: ElementChild;
  styleType?: 'default';
  childrenAlign?: 'left' | 'right';
}

@observer
export class Label extends ElementComponent<IFormLabelProps, any> {
  protected baseClass: string = '';

  public render() {
    return <label {...this.attributes} className={this.className}>
      { this.props.childrenAlign === 'left' ? this.props.children : null }
      { this.props.label }
      { this.props.childrenAlign === 'right' ? this.props.children : null }
    </label>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Style type
      this.props.styleType === 'default' ? 'uk-form-label' : '',
    ]);
  }
}

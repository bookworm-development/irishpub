import { computed, FormComponent, h, IBaseFormComponent, observer } from '../../core';

export interface ITextareaProps extends IBaseFormComponent {
  placeholder?: string;

  cols?: number;
  rows?: number;
  spellcheck?: boolean;
  wrap?: 'true' | 'soft' | 'off';
}

@observer
export class Textarea extends FormComponent<ITextareaProps, any> {
  protected baseClass: string = 'uk-textarea';

  public render() {
    return <textarea
      className={this.className} {...this.attributes}
      name={this.props.name} placeholder={this.props.placeholder || ''} value={this.props.value as string}  />;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Cols
      ...this.props.cols ? { cols: this.props.cols } : {},

      // Rows
      ...this.props.rows ? { rows: this.props.rows } : {},

      // Spellcheck
      ...this.props.spellcheck ? { spellcheck: this.props.spellcheck } : {},

      // Wrap
      ...this.props.wrap ? { wrap: this.props.wrap } : {},
    };
  }
}

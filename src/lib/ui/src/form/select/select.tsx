import { computed, FormComponent, h, IBaseFormComponent, observer } from '../../core';

export interface ISelectProps extends IBaseFormComponent {
  options: IOption[];
  multiple?: boolean;
  pristine?: IOption;
}

export interface IOption {
  label?: string;
  value?: any;
}

@observer
export class Select extends FormComponent<ISelectProps, any> {
  protected baseClass: string = 'uk-select';

  public render() {
    return <select className={this.className} {...this.attributes}
      value={this.props.value as any}>
        {
          this.props.pristine ?
            <option value={this.props.pristine.value || ''}>{this.props.pristine.label}</option> : null
        }
        {
          this.props.options.map((o) => <option value={o.value}>{o.label}</option>)
        }
      </select>;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Multiple
      ...this.props.multiple ? { multiple: true } : {},
    };
  }
}

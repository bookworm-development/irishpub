import { Icons } from '../..';
import { action, BaseComponent, computed, h, IconType, observable, observer } from '../../core';
import { Link } from '../../link';
import { IInputProps, Input } from './input';

type Diff<T, U> = T extends U ? never : T;
export interface ISensitiveInput extends Diff<IInputProps, { type: any }> {
  icons?: { default: IconType; alt: IconType };
}

@observer
export class SensitiveInput extends BaseComponent<ISensitiveInput, any> {
  protected baseClass: string = 'uk-input';

  @observable
  protected sensitiveState: boolean = true;

  public render() {
    return <Input {...this.props}
      type={this.sensitiveState ? 'password' : 'text'}
      iconElement={this.icon} />;
  }

  @action.bound
  protected sensitiveChange(ev: MouseEvent) {
    ev.preventDefault();
    ev.stopPropagation();

    this.sensitiveState = !this.sensitiveState;
  }

  @computed
  private get icons() {
    return {
      alt: this.props.icons && this.props.icons.alt || Icons.app.lock,
      default: this.props.icons && this.props.icons.default || Icons.app.unlock,
    };
  }

  @computed
  private get icon() {
    return <Link
      class={`uk-form-icon${this.props.iconAlign === 'right' ? ' uk-form-icon-flip' : ''}`}
      location='' tabindex={-1}
      label={
        <Icons.Icon
          icon={this.sensitiveState ? this.icons.default : this.icons.alt}
          forceAttributes={{ onClick: this.sensitiveChange }} />} />;
  }
}

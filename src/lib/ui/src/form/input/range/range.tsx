import { computed, FormComponent, h, IBaseFormComponent, observer } from '../../../core';

export interface IRangeProps extends IBaseFormComponent {
  min?: number;
  max: number;
  step?: number;
  thickMarks?: boolean;
  thickMarkLabels?: { [markIdx: number]: string; };
}

@observer
export class Range extends FormComponent<IRangeProps, any> {
  private static currentId = 0;

  protected baseClass: string = 'uk-range';

  private id: string;

  public componentWillMount() {
    this.id = `tickmarks-${Range.currentId++}`;
  }

  public render() {
    return [
      <input className={this.className} {...this.attributes}
        type='range' name={this.props.name} value={this.props.value as number} />,

      this.props.thickMarks ?
        <datalist id={this.id}>
          { this.marks }
        </datalist> : null,
    ];
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Min
      min: this.props.min || 0,

      // Max
      max: this.props.max,

      // Step
      ...this.props.step ? { step: this.props.step } : {},

      // ThickMarks
      ...this.props.thickMarks ? { list: this.id } : {},
    };
  }

  @computed
  protected get marks() {
    const min = this.props.min || 0;
    const max = this.props.max;
    const labels = this.props.thickMarkLabels || {};

    const step = this.props.step || max - min / 10;
    const steps = [];

    for (let i = min; i <= max; i += step) {
      steps.push(i);
    }

    return steps.map((s, i) => <option value={s} {...labels[i] ? { label: labels[i] } : {}}></option>);
  }
}

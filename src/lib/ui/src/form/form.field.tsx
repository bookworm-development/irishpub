import {
  ElementChild, ElementComponent, h, IBaseElementComponent, observer,
} from '../core';
import { Label } from './form.label';

export interface IFormFieldProps extends IBaseElementComponent {
  label?: ElementChild;
}

@observer
export class Field extends ElementComponent<IFormFieldProps, any> {
  protected baseClass: string = '';

  public render() {
    return <div className={this.className} {...this.attributes}>
      <Label styleType='default' label={this.props.label} />
      <div className='uk-form-controls'>
        {this.props.children}
      </div>
    </div>;
  }
}

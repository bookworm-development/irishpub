import {
  computed, ElementComponent, h, IBaseElementComponent, observer,
} from '../core';

export interface IFormProps extends IBaseElementComponent {
  layout?: 'stacked' | 'horizontal';
  grid?: boolean;
  gridSize?: 'small' | 'medium' | 'large' | 'collapse';
}

@observer
export class Form extends ElementComponent<IFormProps, any> {
  protected baseClass: string = '';

  public render() {
    return <form className={this.className} {...this.attributes}>
      {this.props.children}
    </form>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Layout
      this.props.layout ? `uk-form-${this.props.layout}` : '',

      // Grid size/style
      this.props.gridSize ? `uk-grid-${this.props.gridSize}` : '',
    ]);
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Grid
      ...this.props.grid ? { ['uk-grid']: true } : {},
    };
  }
}

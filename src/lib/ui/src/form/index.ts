import { Label } from './form.label';
import * as AllInputs from './input';
import { Checkbox } from './input/checkbox/checkbox';
import { Radio } from './input/checkbox/radio';
import { Range } from './input/range/range';

export { Form } from './form';
export { IFormProps } from './form';
export { Field } from './form.field';
export const Inputs = {
  ...AllInputs,
  Checkbox,
  Label,
  Radio,
  Range,
};
export { Textarea } from './textarea';

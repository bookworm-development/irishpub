import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface ILabelProps extends IBaseElementComponent {
  styleType?: 'success' | 'warning' | 'danger';
}

@observer
export class Label extends ElementComponent<ILabelProps, any> {
  private static ClassPrefix = 'uk-label-';

  protected baseClass = 'uk-label';

  public render() {
    return <span class={this.className} {...this.attributes}>
      {this.props.children}
    </span>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Style
      this.props.styleType ? `${Label.ClassPrefix}${this.props.styleType}` : '',
    ]);
  }
}

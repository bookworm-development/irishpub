import * as fs from 'fs';
import { sync } from 'glob';
import * as path from 'path';

import { File } from 'fuse-box/core/File';
import { Plugin, WorkFlowContext } from 'fuse-box/core/WorkflowContext';

export interface IAggregateLocalesOptions {
  locales: string[];
  out?: string;
}

interface ILocales {
  [name: string]: ILocales | string | number;
}

// tslint:disable:no-var-requires
const mergeDefaults = require('defaults-deep');
// tslint:enable:no-var-requires

export class FuseBoxAggregateLocalesPlugin implements Plugin {
  public text: RegExp = /$/;

  private configuration: IAggregateLocalesOptions;

  private locales: ILocales = {};

  private log: WorkFlowContext['log'];

  public constructor(options: IAggregateLocalesOptions) {
    const o = options || {};

    this.configuration = {
      locales: o.locales || [],
      out: o.out || 'locales',
    } as IAggregateLocalesOptions;
  }

  public init(context: WorkFlowContext) {
    this.log = context.log;
  }

  public transform(_: File) {
    // Do nothing
  }

  public bundleEnd(context: WorkFlowContext) {
    // Aggregate the locale data on bundle end
    const fuseContext = context.fuse.context;
    const out = path.join(fuseContext.output.dir, this.configuration.out as string);

    const locales = this.configuration.locales.map((l) => `*.locale.${l}.json`).join('|');

    this.log.echoPlain('\n\nLocales aggregation');

    // Search all locale files
    const globs = sync(`**/*(${locales})`, {
      cwd: fuseContext.homeDir,
    });

    globs.forEach((g) => {
      this.log.echoGray(`  ${g}`);

      const pathInfo = path.parse(g);
      const filePath = pathInfo.dir.split(path.sep);
      const fileNameSegments = pathInfo.name.split('.');
      const locale = fileNameSegments[fileNameSegments.length - 1];

      this.locales[locale] = this.locales[locale] || {};

      this.createContainers([...filePath], this.locales[locale] as ILocales);
      const container = this.getContainer(filePath.slice(0, filePath.length - 1), this.locales[locale] as ILocales);
      let contents = {};

      try {
        contents = JSON.parse(
          fs.readFileSync(path.join(fuseContext.homeDir, g)).toString(),
        );
      } catch (e) {
        this.log.echoError(`[${g}]: ${e}`);
      }

      container[filePath[filePath.length - 1]] = {
        ...(container[filePath[filePath.length - 1]] as ILocales || {}),
        ...contents,
      };
    });

    try {
      fs.mkdirSync(out);
    } catch (e) {
      // Do nothing here
    }

    this.configuration.locales.map((l) => {
      return this.locales[l];
    }).forEach((l, i, all) => {
      const locale = this.configuration.locales[i];

      fs.writeFileSync(
        path.join(out, `${locale}.json`),
        JSON.stringify(
          mergeDefaults(l as ILocales, ...all as ILocales[]),
          null,
          2,
        ),
      );
    });

    this.log.echoYellow('  Done');
  }

  // Creates the hierarchy of containers required to store locales data
  private createContainers(segments: string[], root: ILocales = this.locales) {
    if (segments.length) {
      const first = segments[0];

      if (!root[first]) {
        root[first] = {};
      }

      segments.shift();

      root[first] = this.createContainers(segments, root[first] as ILocales);
    }

    return root;
  }

  private getContainer(segments: string[], container: ILocales = this.locales): ILocales {
    return !segments.length ?
      container :
      this.getContainer(segments.slice(1), container[segments[0]] as ILocales);
  }
}

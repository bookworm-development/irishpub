declare module 'defaults-deep' {
  interface IAnyHash {
    [p: string]: any;
    [n: number]: any;
  }

  function defaultsDeep(target: IAnyHash, ...objects: IAnyHash[]): IAnyHash;

  export default defaultsDeep;
}

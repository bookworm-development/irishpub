import { FuseBoxAggregateLocalesPlugin, IAggregateLocalesOptions } from './fuse.aggregate-locales';

export const AggregateLocalesPlugin = (options: IAggregateLocalesOptions = { locales: ['en'] }) => {
  return new FuseBoxAggregateLocalesPlugin(options);
};

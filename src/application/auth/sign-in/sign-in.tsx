import { Component, h } from 'preact';

import { Text } from '../../../core/intl';
import { observer, computed, action, observable, inject } from '../../../core/store';
import { Containers, Icons, Logo, Images, Buttons, Headings } from '../../../lib/ui/src';
import { Animation } from '../../../lib/ui/src/core/animation';
import { SignInMethods, SignInStore } from './sign-in.form.store';
import { SignInForm } from './sign-in.form';
import { Notifications } from '../../../lib/ui/src';

import * as logo from '../../../assets/logo.svg';
import './sign-in.form.store';
import './sign-in.scss';

const DefaultSignInButtons: SignInMethods[] = ['facebook', 'google', 'twitter', 'email'];
const DefaultSignInButtonClasses: { [m in SignInMethods]: string } = {
  'facebook': 'facebook-button',
  'google': 'google-button',
  'twitter': 'twitter-button',
  'email': '',
};

interface ISignInLayoutProps {
  signin: SignInStore;
}

@inject('signin')
@observer
export class SignInLayout extends Component<ISignInLayoutProps, any> {
  private static SuccessfulLoginRedirectAddr = 'https://www.facebook.com/IrishMusicPubCluj';

  @observable
  private buttonsContainerOptions: { animation?: Animation | Animation[] } | null = null;

  @observable
  private formContainerOptions: { animation?: Animation | Animation[] } | null = null;

  @observable
  private emailLogin: boolean = false;

  @observable
  private loggedIn: boolean = false;

  public render() {
    return <Containers.Grid size='large' height='viewport' animation={['fade', 'slide-top-medium']}>
      <Containers.Section height='viewport' flex={['center', 'middle']} width='1-1'>
        <Containers.Grid size='collapse' width={['2-3@xl', '4-5@l', '4-5@m', '5-6@s', 'child-1-2@m', 'child-1-1@s']} flex={['center']}>
          <Logo location='/' width='large'>
            <Images.Image src={logo} style={{ width: '100%' }} flex='center' />
          </Logo>

          <Containers.Panel padding='large' style={{ alignSelf: 'center' }} width='large'>
            {
              !this.props.signin.signInMethod || this.buttonsContainerOptions ?
                <Containers.Grid size='small' width='child-1-1' flex='middle' {...this.buttonsContainerOptions}
                  onAnimationEnd={this.clearButtonsContainerOptions}>
                  <Headings.H4 text={['center@s', 'left@m']}>
                    <Text id='auth.sign-in.title' />
                  </Headings.H4>

                  <Containers.Div text={['meta', 'center@s', 'left@m']}>
                    <Text id='auth.sign-in.announcementProviders' />
                  </Containers.Div>

                  {
                    this.signInButtons
                  }
                </Containers.Grid> :
                this.props.signin.signInMethod && this.emailLogin ?
                  <Containers.Container {...this.formContainerOptions}
                    onAnimationEnd={this.clearFormContainerOptions}>
                    <Headings.H4 text={['center@s', 'left@m']}>
                      <Text id='auth.sign-in.title' />
                    </Headings.H4>

                    <Containers.Div text={['meta', 'center@s', 'left@m']}>
                      <Text id='auth.sign-in.announcementManual' />
                    </Containers.Div>

                    <SignInForm actionTraps={{ submitBtn: { after: this.afterLogin } }} />

                    <Buttons.Button width='1-1' margin='large-top'
                      label={<Text id='auth.sign-in.allMethods' />}
                      icon={Icons.direction['arrow-left']}
                      handler={this.showAllMethods} styleType='text' />
                  </Containers.Container> :
                  this.loggedIn ?
                    <Containers.Card width='large'>
                      <Containers.Grid size='small'>
                        <Icons.Icon ratio={3.5} icon={Icons.app.check} text='success'
                          animation={['scale-up']} onAnimationEnd={this.loginDone} />

                        <Containers.Div width='expand' flex='middle'>
                          <Text id={`auth.sign-in.methods.success`} fields={{ method: this.props.signin.signInMethod }} />
                        </Containers.Div>
                      </Containers.Grid>
                    </Containers.Card> :
                    <Containers.Card width='large'>
                      <Containers.Grid size='small'>
                        <Icons.Icon ratio={3.5} icon={Icons.brands[this.props.signin.signInMethod as any] as any}
                          animation={['shake']} onAnimationEnd={this.delayRepeat} style={{ animationDelay: '500ms' }} />

                        <Containers.Div width='expand' flex='middle'>
                          <Text id={`auth.sign-in.methods.description`} fields={{ method: this.props.signin.signInMethod }} />
                        </Containers.Div>
                      </Containers.Grid>
                    </Containers.Card>
            }
          </Containers.Panel>
        </Containers.Grid>
      </Containers.Section>
    </Containers.Grid>;
  }

  @computed
  private get signInButtons() {
    return DefaultSignInButtons.map((m) => {
      const isBrand = m in Icons.brands;

      return <Buttons.Button margin='small-left'
        label={<Text id={`auth.sign-in.methods.labels.${m}`} />}
        class={DefaultSignInButtonClasses[m]}
        handler={() => this.showSignInAs(m)}
        {
          ...isBrand ?
            {
              icon: Icons.brands[m as any] as any,
              inverse: 'light',
            } : {
              icon: Icons.app['sign-out'],
              styleType: 'text',
              margin: 'medium-top',
            }
        }
        />;
    });
  }

  @action.bound
  private async showSignInAs(method: SignInMethods) {
    const isBrand = method in Icons.brands;
    this.props.signin.signInMethod = method;

    if (isBrand) {
      // Allow the login providers to do thier jobs
      const loggedIn = await this.props.signin.login(null as any);

      return this.afterLogin(loggedIn);
    } else {
      this.emailLogin = true;
    }

    this.buttonsContainerOptions =  { animation: ['slide-bottom', 'reverse'] };
  }

  @action.bound
  private afterLogin(loggedIn: boolean | Error | undefined) {
    const isBrand = this.props.signin.signInMethod !== 'email';

    if (isBrand) {
      if (!loggedIn || loggedIn instanceof Error) {
        // Notify in case of errors
        if (loggedIn && (loggedIn as any as  { code: string }).code === 'auth/popup-closed-by-user') {
          Notifications.new({ group: 'signin', message: loggedIn.message, status: 'danger' });
        }

        // And move back to the main screen
        this.props.signin.signInMethod = null;

        this.buttonsContainerOptions = { animation: ['slide-bottom'] };
      } else {
        // Mark successfull logins
        this.loggedIn = true;
      }
    } else {
      // Email login
      if (loggedIn && !(loggedIn instanceof Error)) {
        this.emailLogin = false;
        this.loggedIn = true;
      }
    }
  }

  @action.bound
  private async showAllMethods() {
    this.emailLogin = false;
    this.props.signin.signInMethod = null;

    this.buttonsContainerOptions =  { animation: ['slide-bottom'] };
  }

  @action.bound
  private clearButtonsContainerOptions() {
    this.buttonsContainerOptions = null;

    if (this.props.signin.signInMethod) {
      this.formContainerOptions = { animation: ['slide-bottom'] };
    }
  }

  @action.bound
  private clearFormContainerOptions() {
    this.formContainerOptions = null;
  }

  @action.bound
  private delayRepeat(ev: Event) {
    const el = ev.target as HTMLElement;

    el.classList.remove('uk-animation-shake');

    setTimeout(() => {
      if (!this.loggedIn) {
        el.classList.add('uk-animation-shake');
      }
    }, 3000);
  }

  @action.bound
  private loginDone() {
    // Route to the desired page
    document.location.replace(SignInLayout.SuccessfulLoginRedirectAddr);
  }
}

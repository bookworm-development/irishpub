import { CustomForm, Field, h, Icons, ICustomFormActions, ICustomFormProps, Inputs } from '../../../core/forms';
import { Text } from '../../../core/intl';
import { computed, inject, observer } from '../../../core/store';
import { IFormStoreFields, SignInStore } from './sign-in.form.store';

import './sign-in.form.store';

interface ISignInFormFormProps extends ICustomFormProps<IFormStoreFields> {
  store: SignInStore;
}

@inject((stores) => ({
  store: stores.signin,
}))
@observer
export class SignInForm extends CustomForm<IFormStoreFields, Partial<ISignInFormFormProps>, any> {
  private static ResetBtnLabelId = 'auth.sign-in.actions.reset';
  private static SubmitBtnLabelId = 'auth.sign-in.actions.submit';
  private static PassFieldLabelId = 'auth.sign-in.fields.password';

  @computed
  protected get actions(): ICustomFormActions {
     return {
      group: true,
      resetBtn: {
        disabled: !this.store.resetAction,
        handler: this.store.reset,
        label: <Text id={SignInForm.ResetBtnLabelId} />,
      },
      submitBtn: {
        disabled: !this.store.submitAction,
        handler: this.store.login,
        label: <Text id={SignInForm.SubmitBtnLabelId} />,
        loading: this.store.submitInProgress,
      },
    };
  }

  @computed
  protected get fields() {
    return [
      <Field label={<Text id={SignInForm.PassFieldLabelId} />}>
        <Inputs.SensitiveInput autocomplete='off'
          { ...this.store.fieldMeta('password') } icon={Icons.app.lock} iconAlign='right' />
      </Field>,
    ];
  }
}

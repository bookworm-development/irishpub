import { action, BaseStore, computed, observable } from '../../../core/store';
import { FormStore, IFieldsValidationRules } from '../../../core/store/form';
import { Notifications } from '../../../lib/ui/src';
import { AuthStore } from '../auth.store';
import { FacebookAuthProvider, GoogleAuthProvider, TwitterAuthProvider } from '../../../firebase';

export interface IFormStoreFields {
  email: string;
  password: string;
}

export type SignInMethods = 'facebook' | 'google' | 'twitter' | 'email';

export class SignInStore extends FormStore<IFormStoreFields> {
  @observable
  private method: SignInMethods | null = null;

  protected validationRules: IFieldsValidationRules = {
    password: {
      maxlength: { maxLength: 20 },
      minlength: { minLength: 5 },
    },
  };

  @action
  public resetFields() {
    this.values = {
      email: {
        loading: false,
        value: 'irishandmusicpub@gmail.com',
      },
      password: {
        loading: false,
        value: '',
      },
    };
  }

  @action.bound
  public async login(ev: MouseEvent | undefined) {
    const auth = BaseStore.getInstane().getStore('auth') as AuthStore;

    if (ev) {
      ev.stopPropagation();
      ev.preventDefault();
    }

    let loginPromise: Promise<boolean | Error>;

    const email = this.values.email.value;
    const passw = this.values.password.value;
    let isBrand = false;

    switch (this.method) {
      case 'email':
        loginPromise = auth.login(email, passw);
        break;

      case 'facebook':
        loginPromise = auth.loginWithProvider(FacebookAuthProvider);
        isBrand = true;
        break;

      case 'google':
        loginPromise = auth.loginWithProvider(GoogleAuthProvider);
        isBrand = true;
        break;

      case  'twitter':
        loginPromise = auth.loginWithProvider(TwitterAuthProvider);
        isBrand = true;
        break;

      default:
        // Nothing to do on unknown or unspecified methods
        return;
    }

    const result = await loginPromise;

    if (!isBrand && (!result || result instanceof Error)) {
      this.values.email.validity = { valid: false };
      this.values.password.validity = { valid: false };

      if (result) {
        this.values.email.validity.reason = result.message;
        this.values.password.validity.reason = result.message;

        Notifications.new({ group: 'auth', message: result.message, status: 'danger' });
      }
    }

    return result;
  }

  @computed
  public get submitInProgress() {
    const auth = BaseStore.getInstane().getStore('auth') as AuthStore;

    return auth.progress;
  }

  @computed
  public get signInMethod() {
    return this.method;
  }
  public set signInMethod(m: SignInMethods | null) {
    this.method = m;
  }
}

// Register an initial instance of the store
BaseStore.registerStoreClass(SignInStore);

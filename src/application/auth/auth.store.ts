import { auth, User, UserInfo } from 'firebase/app';

import { Route } from '../../core/routing';
import { action, BaseStore, computed, observable, runInAction, Store} from '../../core/store';
import { firebaseApp } from '../../firebase';

export class AuthStore extends Store {
  @observable private session: ISession = {} as any;

  @observable private inProgress: boolean = false;

  public constructor() {
    super();

    // Reset the store on initialization
    this.reset();
  }

  public get user() {
    return this.session.user;
  }

  /**
   * Resets the store to it's original blank state
   */
  public reset() {
    this.session = {
      period: {},
      user: this.extractUserInfo({}),
    };
  }

  @computed
  public get progress() {
    return this.inProgress;
  }

  /**
   * Checks if an actively logged in session exists
   */
  @computed
  public get loggedIn(): boolean {
    return !!(
        this.session.period && this.session.period.started && !this.session.period.ended &&
        this.session.token && this.session.user.uid
      );
  }

  @action.bound
  public async login(email: string, password: string): Promise<Error | boolean> {
    this.inProgress = true;

    try {
      const loggedIn = await firebaseApp.auth().signInWithEmailAndPassword(email, password);

      if (loggedIn.user && (loggedIn.user as User).uid) {
        this.onSuccessfulLogin(loggedIn.user);
      } else {
        runInAction(() => { this.inProgress = false; });

        return false;
      }
    } catch (e) {
      runInAction(() => { this.inProgress = false; });

      return new Error(`${e.code}: ${e.message}`);
    }

    runInAction(() => { this.inProgress = false; });

    return true;
  }

  public async loginWithProvider(provider: auth.AuthProvider): Promise<Error | boolean> {
    try  {
      const result = await firebaseApp.auth().signInWithPopup(provider);
      const userCredentials = result as any as auth.UserCredential;

      if (userCredentials.user) {
        this.onSuccessfulLogin(userCredentials.user);
      }

      return true;
    } catch (e) {
      return e;
    }
  }

  @action.bound
  public async logout(): Promise<boolean> {
    try {
      await firebaseApp.auth().signOut();

      this.session.period.ended = new Date();
    } catch (_) {
      return false;
    }

    return true;
  }

  public isRouteAvailable(r: Route) {
    if (!r.noAuthentication && !this.loggedIn) {
      // Do not bother with routes requiring authentication in unnauthenticated contexts
      return false;
    }

    // Check if the role allows listing of the route (as all main routes end in a listing)
    return true;
  }

  /**
   * Given a Firebase User or plain object, extracts the Firebase UserInfo equivalent
   */
  private extractUserInfo(user: User | {}): UserInfo {
    const u = (user || ({} as any)) as User;

    return {
      displayName: u.displayName || '',
      email: u.email || '',
      phoneNumber: u.phoneNumber || '',
      photoURL: u.photoURL || '',
      providerId: u.providerId || '',
      uid: u.uid || '',
    };
  }

  @action.bound
  private onSuccessfulLogin(u: User) {
    this.session.user = this.extractUserInfo(u);
    this.session.period.started = new Date();
    this.session.token = u.refreshToken;
  }
}

export interface ISession {
  period: { started?: Date; ended?: Date; };
  user: UserInfo;
  token?: string;
}

// Register an initial instance of the store
BaseStore.registerStoreClass(AuthStore);

import { createHashHistory } from 'history';
import { Component, h } from 'preact';
import { route, Router, RouterOnChangeArgs } from 'preact-router';

import { Route } from '../../core/routing';
import { action, computed, inject, observer } from '../../core/store';
import { AuthStore } from '../auth/auth.store';
import { routes } from '../routes';

import '../auth/auth.store'; // Import for the side effects

interface IApplicationRouterProps {
  auth?: AuthStore;
}

@inject('auth')
@observer
export class ApplicationRouter extends Component<IApplicationRouterProps, any> {
  private unauthorizedURLAttempt: string;

  public render() {
    return <Router onChange={this.route} history={createHashHistory()}>
      {
        this.routes.map((r) => {
          // Static route
          const C = r.layout as new() => Component<any, any>;

          return <C path={r.path} />;
        })
      }
    </Router>;
  }

  @computed
  private get routes(): Route[] {
    const auth = this.props.auth as AuthStore;

    return [
      // Add the authentication page first
      routes.notAuthorized,

      // Add all top level routes available to the current user and context
      ...routes.routes.filter((r) => auth.isRouteAvailable(r.route)).map((r) => r.route),
    ];
  }

  @action.bound
  private async route(e: RouterOnChangeArgs) {
    const auth = this.props.auth as AuthStore;

    switch (e.url) {
      case '/main':

        break;
      case '/logout':
        return route('/', true);
      default:
        // TODO: Redirect to 404
        break;
    }

    // Check authentication before allowing routing
    if (!auth.loggedIn) {
      if (routes.notAuthorized.path !== e.url) {
        // Remember the intended path to navigate to before redirecting to login
        this.unauthorizedURLAttempt = e.url;
        console.info('User is not logged in, cought an attempt for: ' + this.unauthorizedURLAttempt);

        return route('/', true);
      }
    } else if (this.unauthorizedURLAttempt) {
      // Attempt to forward the user to the previously intended destination after a successful login
      const url = this.unauthorizedURLAttempt;
      this.unauthorizedURLAttempt = '';

      if (this.unauthorizedURLAttempt !== routes.notAuthorized.path) {
        return route(url, true);
      }
    }
  }
}

import { AllRights, IRoutes } from '../core/routing';
import { normalizeRoutes } from '../core/routing/route';
import { SignInLayout } from './auth/sign-in/sign-in';

export const routes: IRoutes = normalizeRoutes({
  notAuthorized: {
    // Use the login layout to allow authentications and authorization
    layout: SignInLayout,
    noAuthentication: true,
    path: '',
    rights: AllRights,
  },
  notFound: {
    // TODO: Change to an actual 404 page later on
    layout: SignInLayout,
    noAuthentication: true,
    path: '',
    rights: AllRights,
  },
  routes: [
  ],
});

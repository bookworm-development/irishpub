import { h, render } from 'preact';

import * as UIKit from 'uikit';
import * as Icons from 'uikit/dist/js/uikit-icons';

import { Application } from './application/application';

UIKit.use(Icons);

const rootElement = document.getElementById('root');

render(<Application />, rootElement as Element);
